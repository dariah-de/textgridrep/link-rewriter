# [1.0.0](https://gitlab.gwdg.de/dariah-de/textgridrep/link-rewriter/compare/v0.5.0...v1.0.0) (2024-03-05)


### Bug Fixes

* increase common version to 6.0.0 release ([5bfd288](https://gitlab.gwdg.de/dariah-de/textgridrep/link-rewriter/commit/5bfd288feff17da96f198ced53ca4c64d5ee61ca))


### Features

* move to jdk17 and jaxb api to jakarta namespace ([9eb7b38](https://gitlab.gwdg.de/dariah-de/textgridrep/link-rewriter/commit/9eb7b384bdff8542e4e6e5f10a00031c1bd213cc))


### BREAKING CHANGES

* dependent projects need jdk >= 17

# [0.5.0](https://gitlab.gwdg.de/dariah-de/textgridrep/link-rewriter/compare/v0.4.2...v0.5.0) (2022-12-09)


### Features

* add new gitlab workflow ([113c0fe](https://gitlab.gwdg.de/dariah-de/textgridrep/link-rewriter/commit/113c0fe255202a438eb7478b226540998e2b1d42))
