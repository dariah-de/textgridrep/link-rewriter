package info.textgrid.utils.export.filenames;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;

/**
 * Parses the (supplied) mime.types file and creates a map of all entries that
 * specify both a MIME type and a file name.
 */
public class FileExtensionMap {

	private final ImmutableMultimap<String, String> map;
	private static java.util.logging.Logger logger = Logger.getLogger(FileExtensionMap.class.getName());

	protected FileExtensionMap() throws IOException {

		// Comments start with # and reach to the end of line.
		// A matching line contains of a mime type and one or more extensions
		// comments are stripped
		// <whitespace>? <mimetype>¹ <whitespace> <extensions>² <whitespace>?
		// <comment>?
		final Pattern mapLine = Pattern.compile("^[ \t]*([^# \t]+)[ \t]*([^#]+)[ \t]*(#.*)?$");
		final Pattern space = Pattern.compile("[ \t]+");
		final LineNumberReader reader = new LineNumberReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(
				"mime.types"), Charset.forName("UTF-8")));
		final Builder<String, String> builder = ImmutableListMultimap.builder();
		String line = reader.readLine();
		while (line != null) {
			final Matcher matcher = mapLine.matcher(line);
			if (matcher.matches() && matcher.groupCount() >= 2) {
				builder.putAll(matcher.group(1), space.split(matcher.group(2)));
			}
			line = reader.readLine();
		}
		map = builder.build();
		logger.fine(MessageFormat.format("Read {0} lines from the mime.types file, parsed {1} mappings", reader.getLineNumber(),
				map.size()));
		reader.close();
	}

	public ImmutableCollection<String> getExtensions(final String mimeType) {
		return map.get(mimeType);
	}

	public Optional<String> getFirstExtension(final String mimeType) {
		final ImmutableCollection<String> extensions = map.get(mimeType);
		if (!extensions.isEmpty())
			return Optional.of(extensions.iterator().next());
		else
			return Optional.absent();
	}

	public String addExtension(final String plainFileName, final String mimeType) {
		final Optional<String> extension = getFirstExtension(mimeType);
		if (extension.isPresent())
			return plainFileName.concat(".").concat(extension.get());
		else
			return plainFileName;
	}

	private static FileExtensionMap instance;

	public static FileExtensionMap getInstance() {
		if (instance == null) {
			try {
				instance = new FileExtensionMap();
			} catch (final IOException e) {
				throw Throwables.propagate(e);
			}
		}
		return instance;
	}

}
