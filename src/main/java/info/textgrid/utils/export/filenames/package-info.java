/**
 * API to manage export filenames for TextGrid objects,
 * including a default implementation.
 * 
 * 
 */
package info.textgrid.utils.export.filenames;