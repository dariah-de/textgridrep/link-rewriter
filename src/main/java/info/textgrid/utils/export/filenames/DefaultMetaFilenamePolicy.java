package info.textgrid.utils.export.filenames;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.aggregations.IAggregationEntry;

import java.net.URI;

import com.google.common.base.Optional;

/**
 * This filename policy augments a given filename policy by appending ".meta"
 * to every generated filename.
 */
public class DefaultMetaFilenamePolicy implements IFilenamePolicy {

	private IFilenamePolicy filenamePolicy;

	public DefaultMetaFilenamePolicy(final IFilenamePolicy filenamePolicy) {
		this.filenamePolicy = filenamePolicy;
	}

	@Override
	public String translate(final String title) {
		return filenamePolicy.translate(title);
	}

	@Override
	public String getFilename(final ObjectType object, final boolean asParent) {
		return filenamePolicy.getFilename(object, asParent).concat(".meta");
	}

	/**
	 * @deprecated Use {@link #getFilename(IAggregationEntry)} instead
	 */
	@Override
	public URI getFilename(final IAggregationEntry entry, final boolean asParent) throws IllegalStateException {
		return getFilename(entry);
	}

	@Override
	public URI getFilename(final IAggregationEntry entry) throws IllegalStateException {
			return URI.create(filenamePolicy.getFilename(entry).toString().concat(".meta"));
	}

	@Override
	public Optional<URI> getBase(final IAggregationEntry entry) {
		return filenamePolicy.getBase(entry);
	}

}
