package info.textgrid.utils.export.filenames;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.aggregations.IAggregationEntry;

import java.net.URI;

import com.google.common.base.Optional;
import com.ibm.icu.text.Transliterator;

/**
 * The default filename policy is a stateless {@link IFilenamePolicy} that
 * creates filenames from objects or aggregation objects according to the
 * following rules:
 * <ol>
 * <li>An arbitrary string is {@linkplain #translate(String) translated} to a
 * filename fragment by applying an ICU based transliteration that converts most
 * scripts to the US-ASCII range. Then, most special characters are replaced by
 * underscore characters.</li>
 * <li>For a {@linkplain ObjectType TextGrid Metadata object}, the
 * {@linkplain #getFilename(IAggregationEntry) generated file name} is
 * created using the pattern <em>‹title0›.‹uripart›[.‹extension›]</em>, where
 * <dl>
 * <dt>title0</dt>
 * <dd>is the first title of the object, {@linkplain #translate(String)
 * translated} according to the rules outlined above,</dd>
 * <dt>uripart</dt>
 * <dd>is the TextGrid URI stripped from its <code>textgrid:</code> scheme,</dd>
 * <dt>extension</dt>
 * <dd>is a filename extension derived using
 * {@link FileExtensionMap#getFirstExtension(String)} from the object's
 * <em>format</em> field. The extension is only included when the
 * <var>asParent</var> parameter is <code>false</code>.</dd>
 * </dl>
 * </li>
 * <li>For an {@linkplain #getFilename(IAggregationEntry) aggregation
 * entry}, the filename is created by first calculating the filename
 * {@linkplain #getFilename(ObjectType, boolean) for the object's metadata}. If
 * there is any {@linkplain #getBase(IAggregationEntry) base URI} for the given
 * entry, the filename is then {@linkplain URI#resolve(String) resolved} against
 * that.</li>
 * <li>the {@linkplain #getBase(IAggregationEntry) base URI} for an aggregation
 * entry is present iff the aggregation entry has a parent aggregation. If it is
 * present, it is the file name for the parent aggregation entry in the
 * <var>asParent</var> flavour with a <code>/</code> appended.
 * </ol>
 *
 * @author Thorsten Vitt
 *
 */
public class DefaultFilenamePolicy implements IFilenamePolicy {


	/**
	 * Since this is a stateless implementation, clients can use the default instance
	 */
	public static final DefaultFilenamePolicy INSTANCE = new DefaultFilenamePolicy();

	/**
	 * These are the ICU transformation rules used for {@link #translate(String)}.
	 */
	protected static String TRANSFORM_RULES = "::Any-Latin;\n"
			+ "Ä } [[:Upper:]] > AE;\n" + "Ä > Ae;\n" + "ä > ae;\n"
			+ "Ö } [[:Upper:]] > OE;\n" + "Ö > Oe;\n" + "ö > oe;\n"
			+ "Ü } [[:Upper:]] > UE;\n" + "Ü > Ue;\n" + "ü > ue;\n"
			+ "ſ > s;\n" + "ẞ > SS;\n" + "::NFD();\n"
			+ "[:Latin:]{[:Mn:]+} > ;\n" + "::NFC();\n" + "Æ > AE;\n"
			+ "Ð > D;\n" + "Ø > O;\n" + "Þ > TH;\n" + "ß > ss;\n" + "æ > ae;\n"
			+ "ð > d;\n" + "ø > o;\n" + "þ > th;\n" + "Đ > D;\n" + "đ > d;\n"
			+ "Ħ > H;\n" + "ħ > h;\n" + "ı > i;\n" + "Ĳ > IJ;\n" + "ĳ > ij;\n"
			+ "ĸ > q;\n" + "Ŀ > L;\n" + "ŀ > l;\n" + "Ł > L;\n" + "ł > l;\n"
			+ "ŉ > \\'n;\n" + "Ŋ > N;\n" + "ŋ > n;\n" + "Œ > OE;\n"
			+ "œ > oe;\n" + "Ŧ > T;\n" + "ŧ > t;\n" + "ſ > s;\n" + "ƀ > b;\n"
			+ "Ɓ > B;\n" + "Ƃ > B;\n" + "ƃ > b;\n" + "Ƈ > C;\n" + "ƈ > c;\n"
			+ "Ɖ > D;\n" + "Ɗ > D;\n" + "Ƌ > D;\n" + "ƌ > d;\n" + "Ɛ > E;\n"
			+ "Ƒ > F;\n" + "ƒ > f;\n" + "Ɠ > G;\n" + "ƕ > hv;\n" + "Ɩ > I;\n"
			+ "Ɨ > I;\n" + "Ƙ > K;\n" + "ƙ > k;\n" + "ƚ > l;\n" + "Ɲ > N;\n"
			+ "ƞ > n;\n" + "Ƣ > OI;\n" + "ƣ > oi;\n" + "Ƥ > P;\n" + "ƥ > p;\n"
			+ "ƫ > t;\n" + "Ƭ > T;\n" + "ƭ > t;\n" + "Ʈ > T;\n" + "Ʋ > V;\n"
			+ "Ƴ > Y;\n" + "ƴ > y;\n" + "Ƶ > Z;\n" + "ƶ > z;\n" + "Ǆ > DZ;\n"
			+ "ǅ > Dz;\n" + "ǆ > dz;\n" + "Ǉ > LJ;\n" + "ǈ > Lj;\n"
			+ "ǉ > lj;\n" + "Ǌ > NJ;\n" + "ǋ > Nj;\n" + "ǌ > nj;\n"
			+ "Ǥ > G;\n" + "ǥ > g;\n" + "Ǳ > DZ;\n" + "ǲ > Dz;\n" + "ǳ > dz;\n"
			+ "ȡ > d;\n" + "Ȥ > Z;\n" + "ȥ > z;\n" + "ȴ > l;\n" + "ȵ > n;\n"
			+ "ȶ > t;\n" + "ȷ > j;\n" + "ȸ > db;\n" + "ȹ > qp;\n" + "Ⱥ > A;\n"
			+ "Ȼ > C;\n" + "ȼ > c;\n" + "Ƚ > L;\n" + "Ⱦ > T;\n" + "ȿ > s;\n"
			+ "ɀ > z;\n" + "Ƀ > B;\n" + "Ʉ > U;\n" + "Ɇ > E;\n" + "ɇ > e;\n"
			+ "Ɉ > J;\n" + "ɉ > j;\n" + "Ɍ > R;\n" + "ɍ > r;\n" + "Ɏ > Y;\n"
			+ "ɏ > y;\n" + "ɓ > b;\n" + "ɕ > c;\n" + "ɖ > d;\n" + "ɗ > d;\n"
			+ "ɛ > e;\n" + "ɟ > j;\n" + "ɠ > g;\n" + "ɡ > g;\n" + "ɢ > G;\n"
			+ "ɦ > h;\n" + "ɧ > h;\n" + "ɨ > i;\n" + "ɪ > I;\n" + "ɫ > l;\n"
			+ "ɬ > l;\n" + "ɭ > l;\n" + "ɱ > m;\n" + "ɲ > n;\n" + "ɳ > n;\n"
			+ "ɴ > N;\n" + "ɶ > OE;\n" + "ɼ > r;\n" + "ɽ > r;\n" + "ɾ > r;\n"
			+ "ʀ > R;\n" + "ʂ > s;\n" + "ʈ > t;\n" + "ʉ > u;\n" + "ʋ > v;\n"
			+ "ʏ > Y;\n" + "ʐ > z;\n" + "ʑ > z;\n" + "ʙ > B;\n" + "ʛ > G;\n"
			+ "ʜ > H;\n" + "ʝ > j;\n" + "ʟ > L;\n" + "ʠ > q;\n" + "ʣ > dz;\n"
			+ "ʥ > dz;\n" + "ʦ > ts;\n" + "ʪ > ls;\n" + "ʫ > lz;\n"
			+ "ᴀ > A;\n" + "ᴁ > AE;\n" + "ᴃ > B;\n" + "ᴄ > C;\n" + "ᴅ > D;\n"
			+ "ᴆ > D;\n" + "ᴇ > E;\n" + "ᴊ > J;\n" + "ᴋ > K;\n" + "ᴌ > L;\n"
			+ "ᴍ > M;\n" + "ᴏ > O;\n" + "ᴘ > P;\n" + "ᴛ > T;\n" + "ᴜ > U;\n"
			+ "ᴠ > V;\n" + "ᴡ > W;\n" + "ᴢ > Z;\n" + "ᵫ > ue;\n" + "ᵬ > b;\n"
			+ "ᵭ > d;\n" + "ᵮ > f;\n" + "ᵯ > m;\n" + "ᵰ > n;\n" + "ᵱ > p;\n"
			+ "ᵲ > r;\n" + "ᵳ > r;\n" + "ᵴ > s;\n" + "ᵵ > t;\n" + "ᵶ > z;\n"
			+ "ᵺ > th;\n" + "ᵻ > I;\n" + "ᵽ > p;\n" + "ᵾ > U;\n" + "ᶀ > b;\n"
			+ "ᶁ > d;\n" + "ᶂ > f;\n" + "ᶃ > g;\n" + "ᶄ > k;\n" + "ᶅ > l;\n"
			+ "ᶆ > m;\n" + "ᶇ > n;\n" + "ᶈ > p;\n" + "ᶉ > r;\n" + "ᶊ > s;\n"
			+ "ᶌ > v;\n" + "ᶍ > x;\n" + "ᶎ > z;\n" + "ᶏ > a;\n" + "ᶑ > d;\n"
			+ "ᶒ > e;\n" + "ᶓ > e;\n" + "ᶖ > i;\n" + "ᶙ > u;\n" + "ẚ > a;\n"
			+ "ẜ > s;\n" + "ẝ > s;\n" + "ẞ > SS;\n" + "Ỻ > LL;\n" + "ỻ > ll;\n"
			+ "Ỽ > V;\n" + "ỽ > v;\n" + "Ỿ > Y;\n" + "ỿ > y;\n" + "ﬀ > ff;\n"
			+ "ﬁ > fi;\n" + "ﬂ > fl;\n" + "ﬃ > ffi;\n" + "ﬄ > ffl;\n"
			+ "ﬅ > st;\n" + "ﬆ > st;\n" + "Ａ > A;\n" + "Ｂ > B;\n" + "Ｃ > C;\n"
			+ "Ｄ > D;\n" + "Ｅ > E;\n" + "Ｆ > F;\n" + "Ｇ > G;\n" + "Ｈ > H;\n"
			+ "Ｉ > I;\n" + "Ｊ > J;\n" + "Ｋ > K;\n" + "Ｌ > L;\n" + "Ｍ > M;\n"
			+ "Ｎ > N;\n" + "Ｏ > O;\n" + "Ｐ > P;\n" + "Ｑ > Q;\n" + "Ｒ > R;\n"
			+ "Ｓ > S;\n" + "Ｔ > T;\n" + "Ｕ > U;\n" + "Ｖ > V;\n" + "Ｗ > W;\n"
			+ "Ｘ > X;\n" + "Ｙ > Y;\n" + "Ｚ > Z;\n" + "ａ > a;\n" + "ｂ > b;\n"
			+ "ｃ > c;\n" + "ｄ > d;\n" + "ｅ > e;\n" + "ｆ > f;\n" + "ｇ > g;\n"
			+ "ｈ > h;\n" + "ｉ > i;\n" + "ｊ > j;\n" + "ｋ > k;\n" + "ｌ > l;\n"
			+ "ｍ > m;\n" + "ｎ > n;\n" + "ｏ > o;\n" + "ｐ > p;\n" + "ｑ > q;\n"
			+ "ｒ > r;\n" + "ｓ > s;\n" + "ｔ > t;\n" + "ｕ > u;\n" + "ｖ > v;\n"
			+ "ｗ > w;\n" + "ｘ > x;\n" + "ｙ > y;\n" + "ｚ > z;\n"
			+ "© > '(C)';\n" + "® > '(R)';\n" + "₠ > CE;\n" + "₢ > Cr;\n"
			+ "₣ > Fr'.';\n" + "₤ > L'.';\n" + "₧ > Pts;\n" + "₹ > Rs;\n"
			+ "℀ > a'_c';\n" + "℁ > a'_s';\n" + "ℂ > C;\n" + "℅ > c'_o';\n"
			+ "℆ > c'_u';\n" + "ℊ > g;\n" + "ℋ > H;\n" + "ℌ > x;\n"
			+ "ℍ > H;\n" + "ℎ > h;\n" + "ℐ > I;\n" + "ℑ > I;\n" + "ℒ > L;\n"
			+ "ℓ > l;\n" + "ℕ > N;\n" + "№ > No;\n" + "ℙ > P;\n" + "ℚ > Q;\n"
			+ "ℛ > R;\n" + "ℜ > R;\n" + "ℝ > R;\n" + "℞ > Rx;\n" + "℡ > TEL;\n"
			+ "ℤ > Z;\n" + "ℨ > Z;\n" + "ℬ > B;\n" + "ℭ > C;\n" + "ℯ > e;\n"
			+ "ℰ > E;\n" + "ℱ > F;\n" + "ℳ > M;\n" + "ℴ > o;\n" + "ℹ > i;\n"
			+ "℻ > FAX;\n" + "ⅅ > D;\n" + "ⅆ > d;\n" + "ⅇ > e;\n" + "ⅈ > i;\n"
			+ "ⅉ > j;\n" + "㍱ > hPa;\n" + "㍲ > da;\n" + "㍳ > AU;\n"
			+ "㍴ > bar;\n" + "㍵ > oV;\n" + "㍶ > pc;\n" + "㍷ > dm;\n"
			+ "㍺ > IU;\n" + "㎀ > pA;\n" + "㎁ > nA;\n" + "㎃ > mA;\n"
			+ "㎄ > kA;\n" + "㎅ > KB;\n" + "㎆ > MB;\n" + "㎇ > GB;\n"
			+ "㎈ > cal;\n" + "㎉ > kcal;\n" + "㎊ > pF;\n" + "㎋ > nF;\n"
			+ "㎎ > mg;\n" + "㎏ > kg;\n" + "㎐ > Hz;\n" + "㎑ > kHz;\n"
			+ "㎒ > MHz;\n" + "㎓ > GHz;\n" + "㎔ > THz;\n" + "㎙ > fm;\n"
			+ "㎚ > nm;\n" + "㎜ > mm;\n" + "㎝ > cm;\n" + "㎞ > km;\n"
			+ "㎧ > m'_s';\n" + "㎩ > Pa;\n" + "㎪ > kPa;\n" + "㎫ > MPa;\n"
			+ "㎬ > GPa;\n" + "㎭ > rad;\n" + "㎮ > rad'_s';\n" + "㎰ > ps;\n"
			+ "㎱ > ns;\n" + "㎳ > ms;\n" + "㎴ > pV;\n" + "㎵ > nV;\n"
			+ "㎷ > mV;\n" + "㎸ > kV;\n" + "㎹ > MV;\n" + "㎺ > pW;\n"
			+ "㎻ > nW;\n" + "㎽ > mW;\n" + "㎾ > kW;\n" + "㎿ > MW;\n"
			+ "㏂ > a'.m.';\n" + "㏃ > Bq;\n" + "㏄ > cc;\n" + "㏅ > cd;\n"
			+ "㏆ > C'_kg';\n" + "㏇ > Co'.';\n" + "㏈ > dB;\n" + "㏉ > Gy;\n"
			+ "㏊ > ha;\n" + "㏋ > HP;\n" + "㏌ > in;\n" + "㏍ > KK;\n"
			+ "㏎ > KM;\n" + "㏏ > kt;\n" + "㏐ > lm;\n" + "㏑ > ln;\n"
			+ "㏒ > log;\n" + "㏓ > lx;\n" + "㏔ > mb;\n" + "㏕ > mil;\n"
			+ "㏖ > mol;\n" + "㏗ > pH;\n" + "㏘ > p'.m.';\n" + "㏙ > PPM;\n"
			+ "㏚ > PR;\n" + "㏛ > sr;\n" + "㏜ > Sv;\n" + "㏝ > Wb;\n"
			+ "㏞ > V'_m';\n" + "㏟ > A'_m';\n" + "⒜ > '(a)';\n" + "⒝ > '(b)';\n"
			+ "⒞ > '(c)';\n" + "⒟ > '(d)';\n" + "⒠ > '(e)';\n" + "⒡ > '(f)';\n"
			+ "⒢ > '(g)';\n" + "⒣ > '(h)';\n" + "⒤ > '(i)';\n" + "⒥ > '(j)';\n"
			+ "⒦ > '(k)';\n" + "⒧ > '(l)';\n" + "⒨ > '(m)';\n" + "⒩ > '(n)';\n"
			+ "⒪ > '(o)';\n" + "⒫ > '(p)';\n" + "⒬ > '(q)';\n" + "⒭ > '(r)';\n"
			+ "⒮ > '(s)';\n" + "⒯ > '(t)';\n" + "⒰ > '(u)';\n" + "⒱ > '(v)';\n"
			+ "⒲ > '(w)';\n" + "⒳ > '(x)';\n" + "⒴ > '(y)';\n" + "⒵ > '(z)';\n"
			+ "Ⅰ > I;\n" + "Ⅱ > II;\n" + "Ⅲ > III;\n" + "Ⅳ > IV;\n"
			+ "Ⅴ > V;\n" + "Ⅵ > VI;\n" + "Ⅶ > VII;\n" + "Ⅷ > VIII;\n"
			+ "Ⅸ > IX;\n" + "Ⅹ > X;\n" + "Ⅺ > XI;\n" + "Ⅻ > XII;\n"
			+ "Ⅼ > L;\n" + "Ⅽ > C;\n" + "Ⅾ > D;\n" + "Ⅿ > M;\n" + "ⅰ > i;\n"
			+ "ⅱ > ii;\n" + "ⅲ > iii;\n" + "ⅳ > iv;\n" + "ⅴ > v;\n"
			+ "ⅵ > vi;\n" + "ⅶ > vii;\n" + "ⅷ > viii;\n" + "ⅸ > ix;\n"
			+ "ⅹ > x;\n" + "ⅺ > xi;\n" + "ⅻ > xii;\n" + "ⅼ > l;\n" + "ⅽ > c;\n"
			+ "ⅾ > d;\n" + "ⅿ > m;\n" + "¼ > '_1_4';\n" + "½ > '_1_2';\n"
			+ "¾ > '_3_4';\n" + "⅓ > '_1_3';\n" + "⅔ > '_2_3';\n"
			+ "⅕ > '_1_5';\n" + "⅖ > '_2_5';\n" + "⅗ > '_3_5';\n"
			+ "⅘ > '_4_5';\n" + "⅙ > '_1_6';\n" + "⅚ > '_5_6';\n"
			+ "⅛ > '_1_8';\n" + "⅜ > '_3_8';\n" + "⅝ > '_5_8';\n"
			+ "⅞ > '_7_8';\n" + "⅟ > '_1_';\n" + "⑴ > '(1)';\n"
			+ "⑵ > '(2)';\n" + "⑶ > '(3)';\n" + "⑷ > '(4)';\n" + "⑸ > '(5)';\n"
			+ "⑹ > '(6)';\n" + "⑺ > '(7)';\n" + "⑻ > '(8)';\n" + "⑼ > '(9)';\n"
			+ "⑽ > '(10)';\n" + "⑾ > '(11)';\n" + "⑿ > '(12)';\n"
			+ "⒀ > '(13)';\n" + "⒁ > '(14)';\n" + "⒂ > '(15)';\n"
			+ "⒃ > '(16)';\n" + "⒄ > '(17)';\n" + "⒅ > '(18)';\n"
			+ "⒆ > '(19)';\n" + "⒇ > '(20)';\n" + "⒈ > 1'.';\n" + "⒉ > 2'.';\n"
			+ "⒊ > 3'.';\n" + "⒋ > 4'.';\n" + "⒌ > 5'.';\n" + "⒍ > 6'.';\n"
			+ "⒎ > 7'.';\n" + "⒏ > 8'.';\n" + "⒐ > 9'.';\n" + "⒑ > 10'.';\n"
			+ "⒒ > 11'.';\n" + "⒓ > 12'.';\n" + "⒔ > 13'.';\n" + "⒕ > 14'.';\n"
			+ "⒖ > 15'.';\n" + "⒗ > 16'.';\n" + "⒘ > 17'.';\n" + "⒙ > 18'.';\n"
			+ "⒚ > 19'.';\n" + "⒛ > 20'.';\n" + "〇 > 0;\n" + "０ > 0;\n"
			+ "１ > 1;\n" + "２ > 2;\n" + "３ > 3;\n" + "４ > 4;\n" + "５ > 5;\n"
			+ "６ > 6;\n" + "７ > 7;\n" + "８ > 8;\n" + "９ > 9;\n" + "  > '_';\n"
			+ "  > '_';\n" + "  > '_';\n" + "  > '_';\n" + "  > '_';\n"
			+ "  > '_';\n" + "  > '_';\n" + "  > '_';\n" + "  > '_';\n"
			+ "  > '_';\n" + "  > '_';\n" + "　 > '_';\n"
			+ "{ [^\\r\\n,0-9A-Za-z\\$\\(\\)!._-] } > \\_\n" + "";
	private static Transliterator transliterator;

	/**
	 * Returns (and creates on demand) an ICU transliterator as used in {@link #translate(String)}
	 * @return ICU transliterator
	 */
	protected static Transliterator getTransliterator() {
		if (transliterator == null)
			transliterator = Transliterator.createFromRules("TgFilenames",
					TRANSFORM_RULES, Transliterator.FORWARD);
		return transliterator;
	}

	@Override
	public String translate(final String title) {
		return getTransliterator().transform(title);
	}

	@Override
	public String getFilename(final ObjectType object, final boolean asParent) {
		final String title = translate(object.getGeneric().getProvided()
				.getTitle().get(0));
		final String uri = object.getGeneric().getGenerated().getTextgridUri()
				.getValue().substring(9); // remove "textgrid:"
		final String format = object.getGeneric().getProvided().getFormat();
		final String extension = FileExtensionMap.getInstance()
				.getFirstExtension(format).or("dat");

		final StringBuilder result = new StringBuilder(title);
		result.append('.').append(uri);
		if (!asParent) {
			result.append('.').append(extension);
		} else {
			result.append('/');
		}

		return result.toString();
	}

	@Override
	public URI getFilename(final IAggregationEntry entry) {
		return getFilename(entry, false);
	}

	@Override
	public URI getFilename(final IAggregationEntry entry, final boolean asParent) {
		final String localName = getFilename(entry.getMetadata(), asParent);
		final URI result;
		final Optional<URI> base = getBase(entry);

		if (base.isPresent())
			result = base.get().resolve(localName);
		else
			result = URI.create(localName);
		return result;
	}

	@Override
	public Optional<URI> getBase(final IAggregationEntry entry) {
		if (entry.getParent().isPresent())
			return Optional.of(getFilename(entry.getParent().get(), true));
		else
			return Optional.absent();
	}


}
