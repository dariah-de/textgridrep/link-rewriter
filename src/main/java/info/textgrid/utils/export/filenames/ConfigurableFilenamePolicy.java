package info.textgrid.utils.export.filenames;

import info.textgrid.namespaces.metadata.agent._2010.AgentRoleType;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.aggregations.AggregationEntry;
import info.textgrid.utils.export.aggregations.IAggregation;
import info.textgrid.utils.export.aggregations.IAggregationEntry;
import info.textgrid.utils.export.aggregations.ITransformableAggregationEntry;
import info.textgrid.utils.export.filenames.ConfigurableFilenamePolicy.Builder.UniqueSegment;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.ibm.icu.text.Transliterator;

/**
 * An {@link IFilenamePolicy} that is user-customizable by providing a pattern
 * string.
 *
 * <h3>Pattern Syntax</h3>
 *
 * <p>
 * A pattern string is a string containing <em>patterns</em> enclosed in curly
 * braces. Each pattern starts with a <em>variable</em> and is optionally
 * followed by one or more options, each introduced by a vertical bar (
 * <code>|</code>). Please note that all whitespace is significant.
 * </p>
 *
 * As an example, the string
 * <code>{author|fallback|20}-{title|sep=,}.{uri}.{ext}</code> contains the
 * variables <var>author</var> with the options <code>fallback</code> and
 * <code>20</code>, the variable <var>title</var> with the option
 * <code>sep=,</code>, and the variables <var>uri</uri> and <var>ext</var>, each
 * without any option.
 *
 * <h4>Basic Variables</h4> The following basic variables are available in all
 * policies:
 *
 * <table>
 * <thead>
 * <th>Variable</th>
 * <th>Supported Options</th>
 * <th>Description</th>
 * </thead> <tbody>
 * <tr>
 * <td>author</td>
 * <td><code>fallback</code>, <code>sep=</code><var>String</var>,
 * <var>Number</var>, <code>raw</code></td>
 * <td>The object's author. This tries to find the nearest work object in the
 * aggregation tree and extracts its author or authors.
 * <p>
 * If the <code>fallback</code> option is included and the matching work
 * <em>does not</em> include author fields, use all agents regardless of their
 * role instead.
 * </p>
 * </td>
 * </tr>
 *
 * <tr>
 * <td>title</td>
 * <td><code>sep=</code><var>String</var>, <var>Number</var>, <code>raw</code></td>
 * <td>The object's title or titles.</td>
 * </tr>
 *
 * <tr>
 * <td>uri</td>
 * <td>—</td>
 * <td>The object's TextGrid URI. This only includes the scheme-specific part.</td>
 * </tr>
 *
 * <tr>
 * <td>ext</td>
 * <td>—</td>
 * <td>A filename extension that is suitable for the object's MIME type, or
 * <code>dat</code> if none found. This does <em>not</em> include a leading dot.
 * </td>
 * </tr>
 *
 * <tr>
 * <td>*</td>
 * <td><code>pre=</code><var>String</var> (Default <code>.</code>),
 * <code>post=</code> <var>String</var></td>
 * <td>A filename disambiguation pattern, only inserted if required. If filename
 * disambiguation is on ({@link #setUniqueFilenames(boolean)}),
 * {@link #getFilename(IAggregationEntry)} will first generate a filename
 * candidate with this pattern expanding to the empty string. If this filename
 * has already been used for a <em>different</em> entry, it will re-run the
 * filename generation with this pattern expanding to the empty string for the
 * first object resolving to the candidate and to
 * <u>pre</u>fix + <var>n-1</var> + <u>post</u>fix for every other object. I.e.
 * for three XML documents by Goethe and the pattern
 * <code>{author}*.{ext}</code> you will get <code>Goethe.xml</code>,
 * <code>Goethe.1.xml</code> and <code>Goethe.2.xml</code>.
 *
 * Instead of <code>{*}</code> without options you can also simply write
 * <code>*</code>.
 *
 * </tbody>
 * </table>
 *
 * <strong>If you generate multiple filenames, your pattern should include
 * either <code>{uri}</code> or <code>*</code> or you risk to get te same
 * filename for different objects!</strong>
 *
 * <h4>Subpatterns</h4>
 *
 * <p>Using {@link Builder#subPolicy(String, ConfigurableFilenamePolicy)} it is
 * possible to define a variable that expands to the result of applying a
 * complete configurable policy to the given object. These subpatterns all take
 * the option <code>/</code>, which appends a slash if and only if the subpolicy
 * expands to a non-empty string that does not already end in a slash.</p>
 * <p>As a special case, a policy that has {@link Builder#isParent()} set,
 * automatically has a subpolicy called <code>parent</code> that delegates
 * to the policy itself, so you can build recursive path structures.</p>
 *
 *
 * <p>Here is a typical setup using subpolicies:</p>
 * <pre>
		ConfigurableFilenamePolicy parentPolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{title}")
				.isParent()
				.build();
		ConfigurableFilenamePolicy filenamePolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{author}-{title}.{uri}.{ext}")
				.subPolicy("parent", parentPolicy)
				.build();
		ConfigurableFilenamePolicy metaPolicy = ConfigurableFilenamePolicy
				.builder(".meta/{filename}.meta")
                .subPolicy("parent", parentPolicy)
                .subPolicy("filename", filenamePolicy)
                .build();
 * </pre>
 *
 * <h4>Options</h4>
 *
 * <table>
 *
 * <tr>
 * <td><var>Number</var></td>
 * <td>
 * If you pass any non-negative non-zero integer number as an option, the
 * expanded value of the variable will be trimmed after at most
 * <var>Number</var> characters. Trimming occurs after all other processing
 * steps for the variable.</td>
 * </tr>
 *
 * <tr>
 * <td><code>raw</code></td>
 * <td>Insert the result of this variable as-is, without character sanitization.
 * <p>
 * If you <em>do not</em> include this option, the result of the metadata-based
 * variables will be transcribed from its original characters to a safe subset
 * of US-ASCII characters in order to be safe from all kinds of encoding and
 * filename issues. This tries to do something sensible with, e.g., umlauts and
 * non-latin scripts.
 * </p>
 * </td>
 * </tr>
 *
 * <tr>
 * <td><code>sep=</code><var>String</var></code></td>
 * <td>If present and the respective metadata field contains multiple values,
 * use all values, joined together with the given separator <var>String</var>.
 * Otherwise, only use the first value.</td>
 * </tr>
 *
 * <tr>
 * <td><code>fallback</code></td>
 * <td>See at the corresponding variable descriptions.</td>
 * </tr>
 * </table>
 *
 */
public class ConfigurableFilenamePolicy implements IFilenamePolicy {

	/** A segment represents a part of a filename pattern. */
	protected interface Segment {
		/**
		 * Appends the expansion of this segment for the given entry to the
		 * given builder.
		 */
		public StringBuilder append(final StringBuilder builder,
				IAggregationEntry entry);
	}

	private Iterable<Segment> segments;
	private boolean isParentPolicy;
	private Transliterator transliterator;

	private boolean uniqueFilenames = true;
	private Map<String, Map<URI, Integer>> usedNames = Maps.newHashMap();

	@Override
	public String translate(final String source) {
		return transliterator.transliterate(source);
	}

	@Override
	public String getFilename(final ObjectType object, final boolean asParent) {
		return getFilename(new AggregationEntry(object, null)).toASCIIString();
	}

	/**
	 * @deprecated Use {@link #getFilename(IAggregationEntry)} instead
	 * @param asParent
	 *            is not supported by {@link ConfigurableFilenamePolicy}
	 * @return Filename URI 
	 */
	@Deprecated
	@Override
	public URI getFilename(final IAggregationEntry entry, final boolean asParent) {
		return getFilename(entry);
	}

	@Override
	public URI getFilename(IAggregationEntry entry) {
		if (isParentPolicy) {
			if (entry.getParent().isPresent())
				entry = entry.getParent().get();
			else
				return URI.create("");
		}

		final StringBuilder builder = new StringBuilder();
		for (final Segment segment : segments)
			segment.append(builder, entry);
		String path = builder.toString();

		if (uniqueFilenames) {
			if (usedNames.containsKey(path)) {
				final StringBuilder uniqueBuilder = new StringBuilder();
				for (final Segment segment : segments) {
					if (segment instanceof UniqueSegment) {
						final UniqueSegment seg = (UniqueSegment) segment;
						seg.append(uniqueBuilder, entry, usedNames.get(path));
					} else
						segment.append(uniqueBuilder, entry);
				}
				path = uniqueBuilder.toString();
			} else {
				final HashMap<URI, Integer> uriMap = Maps.newHashMap();
				uriMap.put(entry.getTextGridURI(), 0);
				usedNames.put(path, uriMap);
			}
		}

		try {
			return new URI(null, null, path, null);
		} catch (final URISyntaxException e) {
			throw new IllegalArgumentException(
					"The path that this filename policy resolved to, " + path
							+ ", cannot be used in an URI.", e);
		}
	}

	@Override
	public Optional<URI> getBase(final IAggregationEntry entry) {

		final URI filename = getFilename(entry);
		final URI resolved = filename.resolve(".");
		if (resolved == null || resolved.getPath().isEmpty()
				|| ".".equals(resolved.getPath()))
			return Optional.absent();
		return Optional.fromNullable(resolved);
	}

	public boolean isUniqueFilenames() {
		return uniqueFilenames;
	}

	/**
	 * Guarantee a unique object to filename mapping.
	 *
	 * @param uniqueFilenames
	 *            , default value is <code>true</code>
	 * @return the configurable filename policy
	 */
	public ConfigurableFilenamePolicy setUniqueFilenames(
			final boolean uniqueFilenames) {
		this.uniqueFilenames = uniqueFilenames;
		return this;
	}

	public static Builder builder(final String pattern) {
		return new Builder(pattern);
	}

	public static class Builder {

		private String pattern;
		private boolean parent;
		private Transliterator transliterator;
		private Map<String, ConfigurableFilenamePolicy> subPolicies = Maps
				.newHashMap();
		private final ConfigurableFilenamePolicy policy;

		protected Builder(final String pattern) {
			this.pattern = pattern;
			this.policy = new ConfigurableFilenamePolicy();
		}

		public Builder isParent() {
			this.parent = true;
			return this;
		}

		public Builder transliterator(final Transliterator transliterator) {
			this.transliterator = transliterator;
			return this;
		}

		public Builder subPolicy(final String name,
				final ConfigurableFilenamePolicy policy) {
			subPolicies.put(name, policy);
			return this;
		}

		public ConfigurableFilenamePolicy build() {
			if (pattern == null)
				throw new IllegalArgumentException(
						"Cannot build a configurable filename policy without a pattern.");
			if (transliterator == null)
				transliterator = DefaultFilenamePolicy.getTransliterator();
			policy.setIsParentPolicy(parent);
			policy.setTransliterator(transliterator);
			if (parent && !subPolicies.containsKey("parent"))
				subPolicies.put("parent", policy);
			final ImmutableList<Segment> segments = parsePattern();
			policy.setSegments(segments);
			return policy;
		}

		// ##################### Builder Implementation
		// ###############################

		private Segment parseVariable(final StringCharacterIterator iter) {
			if (iter.current() == '{')
				iter.next();
			final StringBuilder buf = new StringBuilder();
			for (char c = iter.current(); c != '}'
					&& c != CharacterIterator.DONE; c = iter.next())
				buf.append(c);
			final String[] split = buf.toString().split("[|]");
			final String var = split[0];
			final String[] options = Arrays.copyOfRange(split, 1, split.length);

			if (subPolicies.containsKey(var))
				return new SubPolicySegment(subPolicies.get(var), options);
			else if (("uri").equals(var))
				return new URISegment(options);
			else if (("title".equals(var)))
				return new TitleSegment(options);
			else if (("ext".equals(var)))
				return new ExtensionSegment(options);
			else if (("author".equals(var)))
				return new AuthorSegment(options);
			else if (("*".equals(var)))
				return new UniqueSegment(options);
			else
				return new StaticSegment("{" + buf.toString() + "}");
		}

		protected ImmutableList<Segment> parsePattern() {
			StringBuilder buffer = new StringBuilder();
			final ImmutableList.Builder<Segment> segments = ImmutableList
					.builder();
			boolean escape = false;

			final StringCharacterIterator iter = new StringCharacterIterator(
					pattern);

			for (char c = iter.first(); c != CharacterIterator.DONE; c = iter
					.next())
				if (escape) {
					buffer.append(c);
					escape = false;
				} else if (c == '\\')
					escape = true;
				else if (c == '{') {
					final String s = buffer.toString();
					if (!s.isEmpty()) {
						segments.add(new StaticSegment(s));
						buffer = new StringBuilder();
					}
					segments.add(parseVariable(iter));
				} else if (c == '*') {
					segments.add(new UniqueSegment());
				} else
					buffer.append(c);
			if (buffer.length() > 0)
				segments.add(new StaticSegment(buffer.toString()));
			return segments.build();
		}

		public class SubPolicySegment extends MetadataSegment implements
				Segment {

			private final ConfigurableFilenamePolicy policy;
			private boolean addSlash;

			public SubPolicySegment(final ConfigurableFilenamePolicy policy,
					final String... options) {
				super(options);
				this.policy = policy;
				this.raw = true;
			}

			@Override
			protected boolean handleOption(final String option) {
				if ("/".equals(option)) {
					this.addSlash = true;
					return true;
				}
				return super.handleOption(option);
			}

			@Override
			public String getContent(final IAggregationEntry entry) {
				String expansion = policy.getFilename(entry).toString();
				if (addSlash && expansion != null && !expansion.isEmpty()
						&& !expansion.endsWith("/"))
					expansion = expansion.concat("/");
				return expansion;
			}

		}

		/**
		 * A segment that expands to a filename extension depending on the entry's format.
		 * 
		 * This will look up the MIME type of the object in the {@link FileExtensionMap} and
		 * append the first extension found (or "dat").
		 * 
		 * If the entry to format is an {@link ITransformableAggregationEntry}, this segment
		 * will expand to the <em>transformed</em> format (if applicable). This behaviour 
		 * can be supressed using the {@code original} option.
		 *
		 */
		public class ExtensionSegment extends MetadataSegment implements
				Segment {

			private boolean original = false;

			public ExtensionSegment(final String... options) {
				super(options);
				raw = true; // extensions need never be transliterated.
			}

			@Override
			protected boolean handleOption(String option) {
				if ("original".equals(option)) {
					original  = true;
					return true;
				}
				return super.handleOption(option);
			}

			@Override
			public String getContent(final IAggregationEntry entry) {
				final String format;
				if (!original && entry instanceof ITransformableAggregationEntry)
					format = ((ITransformableAggregationEntry) entry).getFinalFormat();
				else
					format = entry.getMetadata().getGeneric()
						.getProvided().getFormat();
				return FileExtensionMap.getInstance().getFirstExtension(format)
						.or("dat");
			}

		}

		/**
		 * A segment that always expands to a static string, regardless of the
		 * entry
		 */
		protected class StaticSegment implements Segment {
			protected final String content;

			/**
			 * Creates a static string segment.
			 *
			 * @param content
			 *            The string this segment will expand to.
			 */
			public StaticSegment(final String content) {
				this.content = content;
			}

			@Override
			public StringBuilder append(final StringBuilder builder,
					final IAggregationEntry entry) {
				return builder.append(content);
			}

		}

		/**
		 * Base class for segments that extract metadata from the given
		 * argument.
		 */
		protected abstract class MetadataSegment implements Segment {
			/**
			 * Hint to insert the raw content of the metadata field, without any
			 * translation to safe characters.
			 */
			protected boolean raw = false;

			/**
			 * Hint that suggests the maximal number of characters to append for
			 * this segment. Values <= 0 mean no restriction.
			 */
			protected int maxlength = -1;

			/**
			 * Initializes this segment. Calls {@link #handleOption(String)} for
			 * each given option.
			 */
			public MetadataSegment(final String... options) {
				for (final String option : options)
					handleOption(option);
			}

			/**
			 * Parse the given option. Clients may override or extend.
			 *
			 * @param option
			 *            The option. Currently supported strings are
			 *            <code>raw</code> to set the {@link #raw} flag or any
			 *            positive decimal integer to set the {@link #maxlength}
			 *            option.
			 * @return <code>true</code> iff the method handled the option
			 */
			protected boolean handleOption(final String option) {
				if ("raw".equals(option))
					raw = true;
				else if (option.matches("\\d+"))
					maxlength = Integer.parseInt(option);
				else
					return false;
				return true;

			}

			/** Return the (unprocessed) string for this entry. */
			public abstract String getContent(IAggregationEntry entry);

			/**
			 * {@inheritDoc}
			 *
			 * The {@link MetadataSegment} implementation calls
			 * {@link #getContent(IAggregationEntry)} and postprocesses the
			 * result according to the {@link #raw} and {@link #maxlength}
			 * fields before appending.
			 */
			@Override
			public StringBuilder append(final StringBuilder builder,
					final IAggregationEntry entry) {
				String content = getContent(entry);
				if (!raw)
					content = transliterator.transform(content);
				if (maxlength > 0)
					content = content.substring(0, Math.min(content.length(), maxlength));
				return builder.append(content);
			}
		}

		public class UniqueSegment extends MetadataSegment {

			private String pre = ".";
			private String post = "";

			public UniqueSegment(final String... options) {
				super(options);
			}

			@Override
			public String getContent(final IAggregationEntry entry) {
				return "";
			}

			/**
			 * Appends a unifying segment if required. Behaviour depends on the
			 * following conditions:
			 *
			 * <ul>
			 * <li>if uriMap is <code>null</code>, nothing is appended.
			 * <li>if uriMap already contains the entry's URI, the
			 * disambiguation number is taken from the uriMap.
			 * <li>if uriMap does not yet contain the entry's URI, the
			 * disambiguation number is the number of items already registered
			 * in the map, and it is registered for the URI in the map.
			 * </ul>
			 * If the disambiguation number has been determined and is > 0, it
			 * is appended together with the registered pre- and postfix.
			 *
			 *
			 * @param builder
			 *            The builder to append to
			 * @param entry
			 * @param uriMap
			 *            A (modifiable) map mapping URIs to disambiguation
			 *            numbers, 0 for none.
			 * @return string builder
			 */
			public StringBuilder append(final StringBuilder builder,
					final IAggregationEntry entry,
					final Map<URI, Integer> uriMap) {
				if (uriMap != null) {
					final URI uri = entry.getTextGridURI();
					Integer count;
					if (uriMap.containsKey(uri)) {
						count = uriMap.get(uri);
					} else {
						count = uriMap.size();
						uriMap.put(uri, count);
					}
					if (count > 0)
						builder.append(pre).append(count).append(post);
				}
				return builder;
			}

			@Override
			protected boolean handleOption(final String option) {
				final String[] split = option.split("=", 2);
				if (split.length < 2)
					return false; // we only support key=value options
				else if ("pre".equals(split[0]))
					this.pre = split[1];
				else if ("post".equals(split[0]))
					this.post = split[1];
				else
					return false;
				return true;
			}

			@Override
			public StringBuilder append(final StringBuilder builder,
					final IAggregationEntry entry) {
				return append(builder, entry, null);
			}

		}

		/**
		 * Formats an object's TextGrid URI.
		 */
		public class URISegment extends MetadataSegment implements Segment {

			public URISegment(final String... options) {
				super(options);
			}

			@Override
			public String getContent(final IAggregationEntry entry) {
				return entry.getMetadata().getGeneric().getGenerated()
						.getTextgridUri().getValue();
			}

		}

		/**
		 * Extracts the title.
		 *
		 * If the additional option sep=<string> is given and the given object
		 * has multiple
		 *
		 */
		public abstract class ListSegment extends MetadataSegment {

			protected Optional<String> separator = Optional.absent();

			public ListSegment(final String... options) {
				super(options);
			}


			@Override
			protected boolean handleOption(final String option) {
				if (super.handleOption(option))
					return true;
				else {
					final Pattern SEP = Pattern.compile("^sep=(.+)$");
					final Matcher matcher = SEP.matcher(option);
					if (matcher.matches()) {
						separator = Optional.of(matcher.group(1));
						return true;
					}
					return false;
				}
			}

			public abstract Iterable<String> getContentItems(
					final IAggregationEntry entry);

			@Override
			public String getContent(final IAggregationEntry entry) {
				final Iterable<String> items = getContentItems(entry);
				if (items == null)
					return "";
				else if (separator.isPresent())
					return Joiner.on(separator.get()).join(items);
				else {
					final Iterator<String> iter = items.iterator();
					if (iter.hasNext())
						return iter.next();
					else
						return "";
				}
			}
		}

		public class TitleSegment extends ListSegment {

			@Override
			public Iterable<String> getContentItems(
					final IAggregationEntry entry) {
				return entry.getMetadata().getGeneric().getProvided()
						.getTitle();
			}

			public TitleSegment(final String... options) {
				super(options);
			}

		}

		public class AuthorSegment extends ListSegment {

			private final Function<AgentType, String> AGENT_NAME = new Function<AgentType, String>() {

				@Override
				public String apply(final AgentType input) {
					return input.getValue();
				}
			};

			public AuthorSegment(final String... options) {
				super(options);
			}

			private boolean fallback;

			@Override
			protected boolean handleOption(final String option) {
				if (super.handleOption(option))
					return true;
				else if ("fallback".equals(option)) {
					this.fallback = true;
					return true;
				}
				return false;
			}

			@Override
			public Iterable<String> getContentItems(
					final IAggregationEntry entry) {
				if (entry.getMetadata().getWork() != null) {
					final List<AgentType> agents = entry.getMetadata()
							.getWork().getAgent();
					final Iterable<AgentType> authors = Iterables.filter(
							agents, new Predicate<AgentType>() {

								@Override
								public boolean apply(final AgentType agent) {
									return AgentRoleType.AUTHOR.equals(agent
											.getRole());
								}
							});
					if (authors.iterator().hasNext()) {
						return Iterables.transform(authors, AGENT_NAME);
					} else if (fallback)
						return Iterables.transform(agents, AGENT_NAME);
				} else if (entry instanceof IAggregation
						&& ((IAggregation) entry).getWork().isPresent()) {
					return getContentItems(((IAggregation) entry).getWork()
							.get());
				} else if (entry.getParent().isPresent())
					return getContentItems(entry.getParent().get());
				return null;
			}

		}

	}

	private void setSegments(final Iterable<Segment> segments) {
		this.segments = segments;
	}

	private void setIsParentPolicy(final boolean isParentPolicy) {
		this.isParentPolicy = isParentPolicy;
	}

	private void setTransliterator(final Transliterator transliterator) {
		this.transliterator = transliterator;
	}

	protected ConfigurableFilenamePolicy() {
	}
}
