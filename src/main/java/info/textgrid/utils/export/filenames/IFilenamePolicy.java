package info.textgrid.utils.export.filenames;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.export.aggregations.IAggregationEntry;

import java.net.URI;

import com.google.common.base.Optional;

/**
 * A filename policy offers a way to generate file names from entries in
 * aggregation structures, from TextGrid metadata, or from simple strings.
 * 
 * <p>
 * Implementors should make sure that the same instance of an
 * {@link IFilenamePolicy} implementation always returns the same filename
 * candidate on the same arguments. Filename stability across different 
 * policy instances is not required.
 * </p>
 * 
 * @see DefaultFilenamePolicy default implementation
 * @author vitt
 * 
 */
public interface IFilenamePolicy {

	/**
	 * Creates a valid file name part for the given string. This may involve
	 * replacing or removing parts of the string, shortening the string etc.
	 * 
	 * @param source
	 *            The string that should be translated
	 * @return A string that conforms to the rules of this policy
	 */
	public String translate(final String source);

	/**
	 * Generates a file name candidate for the object described by the given
	 * metadata.
	 * 
	 * @param object
	 *            The object with the metadata for which to generate a filename
	 * @param asParent
	 *            If true, the generated file name should be the parent for a
	 *            different filename. Implementors might, e.g., want to leave
	 *            out an extension.
	 * @return The generated filename.
	 */
	public String getFilename(final ObjectType object, final boolean asParent);

	/**
	 * Generates a file name for the aggregation entry described by the given
	 * metadata.
	 * 
	 * @see #getFilename(ObjectType, boolean)
	 * @deprecated Use {@link #getFilename(IAggregationEntry)} instead
	 */
	public URI getFilename(final IAggregationEntry entry, final boolean asParent);

	/**
	 * Generates a file name for the aggregation entry described by the given
	 * metadata.
	 * 
	 * @see #getFilename(ObjectType, boolean)
	 */
	public URI getFilename(final IAggregationEntry entry);

	/**
	 * Tries to generate a base URI for the given aggregation entry.
	 */
	public abstract Optional<URI> getBase(final IAggregationEntry entry);

}
