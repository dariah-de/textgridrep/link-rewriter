package info.textgrid.utils.export.aggregations;

/**
 * An aggregation entry that can possibly be transformed to a different kind of
 * format.
 * 
 * @since 0.4.1
 */
public interface ITransformableAggregationEntry extends IAggregationEntry {

	/**
	 * Returns the final format of the entry, after optional transformation.
	 * 
	 * In practice, this means it will return the format given with
	 * {@link #setTransformedFormat(String)} if that has been called before or
	 * the original format from the entry's metadata otherwise.
	 */
	public abstract String getFinalFormat();

	/**
	 * Set the format that the entry would have after transformation.
	 * 
	 * @param format
	 */
	public abstract void setTransformedFormat(String format);

}