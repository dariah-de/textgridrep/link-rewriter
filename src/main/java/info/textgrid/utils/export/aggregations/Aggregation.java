package info.textgrid.utils.export.aggregations;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

public class Aggregation extends AggregationEntry implements IAggregation {

	private final List<IAggregationEntry> children = Lists.newArrayList();
	
	private Optional<IAggregationEntry> work = Optional.absent();

	public Aggregation(final ObjectType metadata) {
		super(metadata);
	}

	public Aggregation(final ObjectType metadata, final Aggregation parent) {
		super(metadata, parent);
	}
	

	// @Override
	// public String toString() {
	// return super.toString() + ": [" + (children == null? "!!!" :
	// Joiner.on(", ").join(children)) + "]";
	// }

	protected IAggregation add(final IAggregationEntry child) {
		children.add(child);
		return this;
	}
	
	/* (non-Javadoc)
	 * @see info.textgrid.utils.export.aggregations.IAggregation#getChildren()
	 */
	@Override
	public ImmutableList<IAggregationEntry> getChildren() {
		return ImmutableList.copyOf(children);
	}

	@Override
	public Optional<? extends IAggregationEntry> getWork() {
		return work;
	}
	
	public void setWork(IAggregationEntry work) {
		this.work = Optional.fromNullable(work);
	}

}
