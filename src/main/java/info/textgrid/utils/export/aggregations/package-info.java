/**
 * Model to represent a set of TextGrid aggregations.
 * 
 * This model consists of an API ({@link info.textgrid.utils.export.aggregations.IAggregation}, {@link info.textgrid.utils.export.aggregations.IAggregationEntry})
 * and a default implementation. Clients who need only read access should use the API interfaces. Clients who need 
 * write access may use the provided default implementation or implement the interfaces themselves.
 * 
 * @see info.textgrid.utils.export.filenames
 */
package info.textgrid.utils.export.aggregations;

