package info.textgrid.utils.export.aggregations;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import java.net.URI;
import java.text.MessageFormat;

import com.google.common.base.Optional;

/**
 * Represents an entry in an aggregation.
 */
public class AggregationEntry implements IAggregationEntry, ITransformableAggregationEntry {

	private final ObjectType metadata;

	protected final Optional<Aggregation> parent;
	
	private Optional<String> transformedFormat = Optional.absent();

	protected AggregationEntry(final ObjectType metadata) {
		this(metadata, null);
	}

	public AggregationEntry(final ObjectType metadata, final Aggregation parent) {
		this.metadata = metadata;
		this.parent = Optional.fromNullable(parent);
		if (this.parent.isPresent()) {
			parent.add(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.utils.export.aggregations.IAggregationEntry#getParent()
	 */
	@Override
	public Optional<Aggregation> getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.utils.export.aggregations.IAggregationEntry#getMetadata()
	 */
	@Override
	public ObjectType getMetadata() {
		return metadata;
	}

	public String getFormat() {
		return getMetadata().getGeneric().getProvided().getFormat();
	}
	
	/* (non-Javadoc)
	 * @see info.textgrid.utils.export.aggregations.ITransformableAggregationEntry#getFinalFormat()
	 */
	@Override
	public String getFinalFormat() {
		if (transformedFormat.isPresent()) {
			return transformedFormat.get();
		} else
			return getFormat();
	}
	
	/* (non-Javadoc)
	 * @see info.textgrid.utils.export.aggregations.ITransformableAggregationEntry#setTransformedFormat(java.lang.String)
	 */
	@Override
	public void setTransformedFormat(final String format) {
		transformedFormat = Optional.of(format);
	}

	@Override
	public String toString() {
		try {
			return MessageFormat.format("{0} ({1}, {2})", metadata.getGeneric().getGenerated().getTextgridUri().getValue(),
					metadata.getGeneric().getProvided().getTitle().get(0), metadata.getGeneric().getProvided().getFormat());
		} catch (final NullPointerException e) {
			return ("<AggregationEntry w/o complete metadata>");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * info.textgrid.utils.export.aggregations.IAggregationEntry#getTextGridURI
	 * ()
	 */
	@Override
	public URI getTextGridURI() {
		return URI.create(getMetadata().getGeneric().getGenerated().getTextgridUri().getValue());
	}

}
