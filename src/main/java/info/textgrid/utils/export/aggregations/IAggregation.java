package info.textgrid.utils.export.aggregations;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

public interface IAggregation extends IAggregationEntry {

	public abstract ImmutableList<? extends IAggregationEntry> getChildren();
	
	public abstract Optional<? extends IAggregationEntry> getWork();

}