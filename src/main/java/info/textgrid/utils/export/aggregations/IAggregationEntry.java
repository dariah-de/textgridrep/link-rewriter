package info.textgrid.utils.export.aggregations;

import info.textgrid.namespaces.metadata.core._2010.ObjectType;

import java.net.URI;

import com.google.common.base.Optional;

public interface IAggregationEntry {

	public abstract Optional<? extends IAggregation> getParent();
	
	public abstract ObjectType getMetadata();

	public abstract URI getTextGridURI();

}