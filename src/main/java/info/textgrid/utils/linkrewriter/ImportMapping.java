package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.RevisionPolicyType;
import info.textgrid._import.RewriteMethod;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.collect.Maps;

/**
 * Maps both TextGrid and local URIs to each other and to the corresponding
 * entries in the {@link ImportSpec}.
 * 
 * <p>This is a convenient and performant adaptor for the part of an {@link ImportSpec}
 * that is merely a list of {@link ImportObject}s. It is used, e.g., by the
 * {@link ConfigurableXMLRewriter}.
 * 
 * <p>Clients can {@linkplain #ImportMapping(ImportSpec) load an import mapping from
 * an ImportSpec} (that can be loaded from a file using JAXB), or they can build
 * a mapping from scratch by calling the {@link #ImportMapping()} constructor 
 * and then {@linkplain #add(ImportObject...) adding} {@link ImportObject}s or
 * using the plain {@link #add(URI, URI, URI, RewriteMethod)} method
 * to add  entries from scratch.
 * 
 * <p>ImportMappings can be {@linkplain #toImportSpec() converted to import specs} 
 * which can then be serialized to XML via JAXB.
 */
public class ImportMapping implements Iterable<ImportObject> {

	private final Map<URI, ImportObject> textgridMap = Maps.newLinkedHashMap();
	private final Map<URI, ImportObject> localMap = Maps.newHashMap();
	private ImportSpec importSpec;
	
	private static Logger logger = Logger.getLogger(ImportMapping.class.getCanonicalName());

	/**
	 * @deprecated Use {@link #getImportObjectForTextGridURI(URI)} instead
	 */
	public ImportObject getImportObjectForTextGridURI(final String textGridURI) {
		return getImportObjectForTextGridURI(makeURI(textGridURI));
	}

	/**
	 * Returns the import object for the given TextGrid URI.
	 */
	public ImportObject getImportObjectForTextGridURI(final URI textGridURI) {
		return textgridMap.get(textGridURI);
	}

	/**
	 * @deprecated Use {@link #getImportObjectForLocalURI(URI)} instead
	 * @see #makeURI(String)
	 */
	public ImportObject getImportObjectForLocalURI(final String localURI) {
		return getImportObjectForLocalURI(makeURI(localURI));
	}

	public ImportObject getImportObjectForLocalURI(final URI localURI) {
		return localMap.get(localURI);
	}

	/**
	 * Adds a bunch of importObjects.
	 */
	public void add(final ImportObject... importObjects) {
		for (final ImportObject importObject : importObjects) {
			textgridMap.put(makeURI(importObject.getTextgridUri()), importObject);
			localMap.put(makeURI(importObject.getLocalData()), importObject);
		}
	}
	
	/**
	 * String-to-URI conversion used by the mapping internally.
	 * 
	 * @throws IllegalArgumentException
	 */
	public static URI makeURI(final String in) throws IllegalArgumentException {
		if (in == null)
			return null;
		
		try {
			return new URI(in);
		} catch (URISyntaxException e) {
			try {
				final URI uri = new URI(null, in, null);
				logger.fine(MessageFormat.format("Input string {0} is not a valid URI, interpreting as file instead -> {1} ({2})", in, uri, e.getReason()));
				return uri;
			} catch (URISyntaxException e1) {
				final String error = MessageFormat.format("Input String \"{0}\" could neither be interpreted as URI ({1}) nor as file name ({2})", in, e.getReason(), e1.getReason());
				logger.log(Level.SEVERE, error , e);
				throw new IllegalArgumentException(error, e);
			}
		}
	}

	/**
	 * Create an {@link ImportObject} on the fly and add it.
	 * @deprecated Use {@link #add(URI,URI,URI,RewriteMethod)} instead
	 */
	public ImportMapping add(final String textgridURI, final String localURI, final String localMetadata,
			final RewriteMethod rewriteMethod) {
				return add(makeURI(textgridURI), makeURI(localURI), makeURI(localMetadata), rewriteMethod);
			}

	/**
	 * Create an {@link ImportObject} on the fly and add it.
	 */
	public ImportMapping add(final URI textgridURI, final URI localURI, final URI localMetadata,
			final RewriteMethod rewriteMethod) {
		final ImportObject importObject = new ImportObject();
		importObject.setTextgridUri(textgridURI.toString());
		importObject.setLocalData(localURI.toString());
		if (localMetadata != null)
			importObject.setLocalMetadata(localMetadata.toString());
		importObject.setRewriteMethod(rewriteMethod);
		add(importObject);

		return this;
	}

	public Collection<? extends ImportObject> getImportObjects() {
		return textgridMap.values();
	}

	@Override
	public Iterator<ImportObject> iterator() {
		return textgridMap.values().iterator();
	}

	public ImportSpec toImportSpec() {
		if (importSpec == null)
			importSpec = new ImportSpec();
		else
			importSpec.getImportObject().clear();

		importSpec.getImportObject().addAll(textgridMap.values());
		return importSpec;
	}

	/**
	 * Initialize an import mapping from a spec that has been loaded.
	 */
	public ImportMapping(final ImportSpec importSpec) {
		this();
		for (final ImportObject object : importSpec.getImportObject())
			add(object);
		this.importSpec = importSpec;
	}

	public RevisionPolicyType getImportRevisionPolicy() {
		if (importSpec != null && importSpec.getRevisionPolicy() != null && importSpec.getRevisionPolicy().getImport() != null)
			return importSpec.getRevisionPolicy().getImport();
		else
			return RevisionPolicyType.ACTUAL;
	}

	public ImportMapping() {
		super();
	}
}
