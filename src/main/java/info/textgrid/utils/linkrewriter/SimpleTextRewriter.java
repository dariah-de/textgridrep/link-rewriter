package info.textgrid.utils.linkrewriter;

import info.textgrid._import.RewriteMethod;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * A simple rewriter for plain-text files. Reads a file line by line and
 * performs simple text replacement from a to b.
 * 
 * @author vitt
 * @see RewriteMethod#TEXT
 * 
 */
public class SimpleTextRewriter extends AbstractRewriter implements ILinkRewriter {

	/**
	 * A rewriter that can rewrite simple text files by replacing everything at
	 * word boundaries.
	 * 
	 * @param mapping
	 *            The mapping between TextGrid URIs and other (local) files.
	 * @param export
	 *            If true, we're exporting from TextGrid (i.e. we need to
	 *            replace TextGrid URIs with local URIs).
	 */
	public SimpleTextRewriter(final ImportMapping mapping, final boolean export) {
		super(mapping, export);
	}

	List<? extends AbstractReplacement> prepareReplacements() {
		return AbstractReplacement.create(WordReplacement.class, getMapping(), isExport());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * The {@link SimpleTextRewriter} implementation rewrites links by simple
	 * text replacements.
	 * 
	 */
	@Override
	public void rewrite(final InputStream input, final OutputStream output) throws IOException {
		final BufferedReader in = new BufferedReader(new InputStreamReader(input));
		final OutputStreamWriter writer = new OutputStreamWriter(output);
		final BufferedWriter out = new BufferedWriter(writer);
		final List<? extends AbstractReplacement> replacements = prepareReplacements();

		for (String line = in.readLine(); line != null; line = in.readLine()) {
			for (final AbstractReplacement replacement : replacements)
				line = replacement.apply(line);
			out.append(line);
		}
		out.close();
		writer.close();

	}

}
