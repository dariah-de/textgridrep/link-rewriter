/**
 * A simple but generic link rewriting library. Currently supports plain-text files and XML files (non-schema aware), both with a rather broad and unconfigurable approach.
 * 
 * <p>{@link info.textgrid.utils.linkrewriter.ImportMapping} contains a 1:1 mapping between objects in TextGrid and other (<em>local</em>) objects, both represented by URIs. Its configuration is available as a (serializable) {@link info.textgrid._import.ImportSpec}.</p>
 * 
 */
package info.textgrid.utils.linkrewriter;

