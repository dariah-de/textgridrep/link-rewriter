package info.textgrid.utils.linkrewriter;

import java.util.regex.Pattern;

/**
 * Replaces the URIs in a string within word boundaries
 * 
 * @author vitt
 * 
 */
class WordReplacement extends AbstractReplacement {
	/**
	 * 
	 */
	private final Pattern pattern;

	@Override
	public String apply(final String in) {
		return pattern.matcher(in).replaceAll(to);
	}

	public WordReplacement(final String from, final String to) {
		super(from, to);
		pattern = preparePattern();
	}

	private Pattern preparePattern() {
		return Pattern.compile(/* "\b" + */Pattern.quote(from) /* + "\b" */);
	}

	@Override
	public String toString() {
		return "WordReplacement [from=" + from + ", to=" + to + ", pattern=" + pattern + "]";
	}

}