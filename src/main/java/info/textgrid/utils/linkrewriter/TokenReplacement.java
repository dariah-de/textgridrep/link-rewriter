package info.textgrid.utils.linkrewriter;

import java.util.StringTokenizer;

/**
 * A replacement that replaces on a token-by-token basis.
 * 
 * @author vitt
 * 
 */
public class TokenReplacement extends AbstractReplacement {

	public TokenReplacement(final String from, final String to) {
		super(from, to);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * The {@link TokenReplacement} implementation first tokenizes the given
	 * string and then checks each token whether it matches either the from
	 * string or the from string + '#' + a fragment. If so, the the replacement
	 * is applied. Everything else is passed in unchanged.
	 */
	@Override
	public String apply(final String in) {

		final StringTokenizer tokenizer = new StringTokenizer(in, " \t\n\r\f", true);
		final StringBuilder result = new StringBuilder(in.length());

		while (tokenizer.hasMoreTokens()) {
			final String token = tokenizer.nextToken();

			if (token.equals(from))
				result.append(to);
			else if (token.startsWith(from + "#"))
				result.append(to).append(token.substring(token.indexOf('#')));
			else
				result.append(token);

		}
		return result.toString();
	}

}
