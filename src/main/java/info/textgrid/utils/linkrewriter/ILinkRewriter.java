package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.stream.XMLStreamException;

/**
 * A link rewriter can rewrite links according to a mapping file.
 * 
 * @see AbstractRewriter
 * @author vitt
 * 
 */
public interface ILinkRewriter {

	/**
	 * Returns the mapping object that describes the neccessary rewrite actions
	 */
	public abstract ImportMapping getMapping();

	/**
	 * If this is true, the rewriting operation is intended for an
	 * <em>export</em> situation, i.e. TextGrid URIs are converted to local
	 * URIs. This is true iff {@link #getImportMode()} is {@link ImportMode#EXPORT}.
	 * 
	 * 
	 * @see #getImportMode()
	 */
	public abstract boolean isExport();

	/**
	 * Copies contents from <var>input</var> to <var>output</var>, rewriting
	 * links according to the {@linkplain #getMapping() mapping} and the
	 * {@linkplain #isExport() export} setting, in an implementation dependent
	 * manner.
	 * 
	 * @param input
	 *            the stream from which the rewriter reads
	 * @param output
	 *            the stream to which the rewriter writes the rewritten code
	 * @throws IOException
	 *             if reading or writing fails
	 * @throws XMLStreamException
	 *             on fatal errors during XML processing.
	 */
	public abstract void rewrite(final InputStream input, final OutputStream output) throws IOException, XMLStreamException;

	public abstract ImportMode getImportMode();

}