package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

/**
 * The base class for replacements used in the link rewriter.
 * 
 * <p>
 * A replacement is responsible for replacing one link in the source file with
 * the destination link.
 * </p>
 * 
 * @author vitt
 * 
 */
public abstract class AbstractReplacement {

	protected final String from;
	protected final String to;

	/**
	 * Creates a replacement that replaces the URL specified by from with the
	 * URL specified by <var>to</var>.
	 * 
	 * <strong>Attention: </strong> All subclasses <em>must</em> implement a
	 * constructor with this signature or creating them using
	 * {@link #create(Class, ImportObject, boolean)} or
	 * {@link #create(Class, ImportMapping, boolean)} fails!
	 */
	public AbstractReplacement(final String from, final String to) {
		super();
		this.from = from;
		this.to = to;
	}

	/**
	 * applies this replacement to the given string.
	 */
	public abstract String apply(final String in);

	/**
	 * Creates a list of replacements that can be applied to strings.
	 * 
	 * @param replacementType
	 *            The class of implementation to use
	 * @param mapping
	 *            The mapping to implement
	 * @param export
	 *            If true, replace from textgrid url to local url
	 * 
	 * @param <T>
	 *            The replacement implementation to use.
	 * @return a list of replacement classes (of the class passed in) applying
	 *         the correct transformation
	 */
	public static <T extends AbstractReplacement> List<T> create(final Class<T> replacementType, final ImportMapping mapping,
			final boolean export) {
		final Collection<? extends ImportObject> objects = mapping.getImportObjects();
		final List<T> result = Lists.newArrayListWithCapacity(objects.size());
		for (final ImportObject object : objects)
			result.add(create(replacementType, object, export));

		return result;
	}

	/**
	 * This is a convenience method that instantiates a conforming replacement
	 * implementation.
	 * 
	 * @param replacementType
	 *            the replacement implementation
	 * @param object
	 *            The mapping object used for the instantiation
	 * @param export
	 *            whether this is an export type replacement (from textgrid: uri
	 *            to local uri)
	 * 
	 * @param <T>
	 *            The type of replacement to instantiate
	 * @return an instance of the class specified by <var>replacementType</var>
	 *         that represents a replacement for this
	 * @throws IllegalArgumentException
	 *             when the class passed in doesn't conform to the specification
	 * @throws IllegalStateException
	 *             when something else goes wrong during instantiation: See the
	 *             nested exception for details.
	 */
	public static <T extends AbstractReplacement> T create(final Class<T> replacementType, final ImportObject object,
			final boolean export) throws IllegalArgumentException, IllegalStateException {
		try {
			final Constructor<T> constructor = replacementType.getConstructor(String.class, String.class);
			final String from;
			final String to;
			if (!export) {
				from = object.getLocalData();
				to = object.getTextgridUri();
			} else {
				from = object.getTextgridUri();
				to = object.getLocalData();
			}
			return constructor.newInstance(from, to);

		} catch (final SecurityException e) {
			throw new IllegalStateException(e);
		} catch (final NoSuchMethodException e) {
			throw new IllegalArgumentException("Replacements must offer a constructor with the argments (String from, String to)",
					e);
		} catch (final InstantiationException e) {
			throw new IllegalStateException(e);
		} catch (final IllegalAccessException e) {
			throw new IllegalStateException(e);
		} catch (final InvocationTargetException e) {
			throw new IllegalStateException(e);
		}
	}

	public static String apply(final List<? extends AbstractReplacement> replacements, String s) {
		for (final AbstractReplacement replacement : replacements) {
			s = replacement.apply(s);
		}
		return s;
	}
}