package info.textgrid.utils.linkrewriter;

import static info.textgrid._import.RevisionPolicyType.LATEST;
import info.textgrid._import.ElementSpec;
import info.textgrid._import.ElementSpec.Required;
import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.NodeRewriteSpec;
import info.textgrid._import.ReplaceMethod;
import info.textgrid._import.XmlConfiguration;
import info.textgrid._import.XmlConfiguration.ProcessingInstruction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import jakarta.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * The configurable XML rewriter allows you to configure where in an XML file
 * link rewriting may occur. You can specify where rewriting will occur and what
 * kind of rewriting will occur there.
 * 
 * <h4>Where to rewrite</h4>
 * 
 * You can specify a list of element names (QNames, that is). For each element
 * name you can specify a rewrite method for the direct text contents and for a
 * list of attribute names. For element and attribute names, we do recognize the
 * special value '*' for <em>any</em> which will match any name in any
 * namespace.
 * 
 * <h4>How to rewrite</h4>
 * <dl>
 * <dt>none (default)</dt>
 * <dd>No rewriting will occur</dd>
 * <dt>full</dt>
 * <dd>rewriting only if the full contents (stripped of surrounding whitespace
 * and a possible fragment) matches</dd>
 * <dt>token</dt>
 * <dd>each token (delimited by whitespace) in here will be looked at</dd>
 * </dl>
 */
public class ConfigurableXMLRewriter extends AbstractRewriter implements
		ILinkRewriter {

	private static final QName XML_ID = new QName(XMLConstants.XML_NS_URI, "id");

	private static final Logger logger = Logger
			.getLogger(ConfigurableXMLRewriter.class.getCanonicalName());

	private Set<String> missingReferences;

	/**
	 * implementers must implement {@link #apply(String)} and can use the other
	 * methods
	 */
	protected abstract class Replacer {

		public abstract String apply(final String to);

		/**
		 * Rewrites a single possible link.
		 * 
		 * @param link
		 *            the URI contained, optionally whitespace-padded
		 *            (whitespace will be stripped). If the link is found in the
		 *            mapping, its replacement is applied (fragment-preserving).
		 * @return rewritten version of <var>link</var> or <code>null</code> if
		 *         <var>link</var> isn't known.
		 */
		protected String rewriteSingleLink(final String link) {
			try {
				final URI uri = new URI(link.trim());
				// Strip fragment
				final String fragment = uri.getRawFragment();
				final String toReplace = fragment == null ? uri.toString()
						: uri.toString()
								.substring(
										0,
										uri.toString().length()
												- fragment.length() - 1);

				if (mergeMode) {
					final String mergedLink = mergeLinkAdjuster.getMergedLink(
							toReplace, fragment);
					if (logger.isLoggable(Level.FINEST))
						logger.finest(MessageFormat.format(
								"Merge mode rewriting {0} to {1}", link,
								mergedLink));
					if (mergedLink == null)
						return link;
					else
						return mergedLink;
				}
				// else

				// try URI replacement
				ImportObject importObject;
				String replacement = null;
				if (isExport()) {
					importObject = getMapping().getImportObjectForTextGridURI(
							toReplace);
					if (importObject == null) {
						final String genericURI = getGenericURI(toReplace);
						if (genericURI != null)
							importObject = getMapping()
									.getImportObjectForTextGridURI(genericURI);
					}
					if (importObject != null) {
						replacement = relativize(importObject.getLocalData());
					}
					if (logger.isLoggable(Level.FINEST))
						logger.finest(MessageFormat
								.format("Export mode rewriting {0} to {1}, relativized from {2}",
										link,
										replacement,
										importObject != null ? importObject
												.getLocalData() : "(not found)"));
				} else {
					final String resolved = resolve(toReplace);
					importObject = getMapping().getImportObjectForLocalURI(
							resolved);
					if (importObject != null) {
						replacement = importObject.getTextgridUri();
						if (getMapping().getImportRevisionPolicy().equals(
								LATEST)) {
							final String genericURI = getGenericURI(replacement);
							if (genericURI != null)
								replacement = genericURI;
						}
					}
					if (logger.isLoggable(Level.FINEST))
						logger.finest(MessageFormat
								.format("Import mode rewriting {0} (resolved {1}) to {2}",
										toReplace, resolved, replacement));
				}
				if (replacement != null) {
					if (fragment == null)
						return replacement;
					else
						return replacement.concat("#").concat(fragment);
				} else if (missingReferences != null && !"".equals(toReplace)) {
					missingReferences.add(toReplace);
					logger.log(Level.FINEST, "Recording missing reference {0}",
							toReplace);
				}
			} catch (final URISyntaxException e) {
				logger.log(Level.FINEST, "Replacer skipping non-URI: " + link,
						e);
			}
			return null;
		}

		/**
		 * Iff <var>textGridURI</var> is a revisioned TextGrid URI return the
		 * generic TextGrid URI, otherwise return <code>null</code>.
		 * 
		 * @param textGridURI
		 *            a revisioned textgrid URI without fragment, e.g.,
		 *            <code>textgrid:15k4.0</code>
		 * @return the generic URI (like <code>textgrid:15k4</code>, or
		 *         <code>null</code>). Also returns <code>null</code> if the
		 *         argument already is generic.
		 */
		private String getGenericURI(final String textGridURI) {
			final int dot = textGridURI.lastIndexOf('.');
			if (dot > 0)
				return textGridURI.substring(0, dot);
			return null;
		}

		private String resolve(final String localData) {
			if (getBase() != null) {
				return getBase().resolve(localData).toString();
			}
			return localData;
		}

		private String relativize(final String localData) {
			if (getBase() != null) {
				final URI localURI = URI.create(localData);
				return ConfigurableXMLRewriter.relativize(getBase(), localURI).toString();
			}
			return localData;
		}
	}

	private final EnumMap<ReplaceMethod, Replacer> replacers;

	private final ListMultimap<QName, ElementSpec> elements = ArrayListMultimap
			.create(15, 1);
	private final Map<QName, XmlConfiguration.ProcessingInstruction> processingInstructions = Maps
			.newHashMap();

	private XmlConfiguration configuration;

	private URI base;

	private boolean mergeMode;

	private IMergeLinkAdjuster mergeLinkAdjuster;

	/**
	 * Returns the currently active base URI.
	 * 
	 * @see #setBase(URI)
	 * @return the current base URI as set by {@link #setBase(URI)}, or
	 *         <code>null</code> if no base handling will take place.
	 */
	public URI getBase() {
		return base;
	}

	/**
	 * Use the given base for the local links. Assume you create the following
	 * structure on the local filesystem:
	 * 
	 * <pre>
	 * foo.xml
	 * sub
	 * +--bar.xml
	 * +--baz.xml
	 * quux.xml
	 * </pre>
	 * 
	 * Now, in your import mapping you will have <em>sub/bar.xml</em> and
	 * <em>sub/baz.xml</em>. However, a link from <em>bar.xml</em> to
	 * <em>baz.xml</em> will be something like
	 * {@code <ref target="baz.xml">Baz</ref>}. The solution is to call
	 * {@code setBase(URI.create("sub/"))} before
	 * {@linkplain #rewrite(InputStream, OutputStream) rewriting} bar.xml and
	 * baz.xml. This will let the rewriter add the given prefix before looking
	 * for matches in the import case, and it will
	 * {@linkplain URI#relativize(URI) relativize} the URIs against the base in
	 * the export case.
	 * 
	 * @param base
	 *            the base URI used from now on in
	 *            {@link #rewrite(InputStream, OutputStream)} operations or
	 *            <code>null</code> (the default) to switch this feature off.
	 */
	public void setBase(final URI base) {
		this.base = base;
	}
	
	/**
	 * From StackOverflow user Alex, http://stackoverflow.com/a/10802188/1719996
	 * #12790
	 */
	public static URI relativize(URI base, URI child) {
		// Normalize paths to remove . and .. segments
		base = base.normalize();
		child = child.normalize();

		// Split paths into segments
		String[] bParts = base.getPath().split("\\/");
		String[] cParts = child.getPath().split("\\/");

		// Discard trailing segment of base path
		if (bParts.length > 0 && !base.getPath().endsWith("/")) {
			bParts = Arrays.copyOf(bParts, bParts.length - 1);
		}

		// Remove common prefix segments
		int i = 0;
		while (i < bParts.length && i < cParts.length
				&& bParts[i].equals(cParts[i])) {
			i++;
		}

		// Construct the relative path
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < (bParts.length - i); j++) {
			sb.append("../");
		}
		for (int j = i; j < cParts.length; j++) {
			if (j != i) {
				sb.append("/");
			}
			sb.append(cParts[j]);
		}

		return URI.create(sb.toString());
	}

	private static List<String> internalSpecs;

	private static Map<URI, XmlConfiguration> internalConfigurations;

	XmlConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * Configures this rewriter for the given configuration. Note that it does
	 * not <em>re</em>configure, i.e., if you need an XML rewriter with a new
	 * configuration, create a new XML rewriter.
	 * 
	 * @param configuration
	 */
	public void configure(final XmlConfiguration configuration) {
		this.configuration = configuration;
		for (final ElementSpec element : configuration.getElement())
			elements.put(element.getName(), element);
		for (final ProcessingInstruction pi : configuration
				.getProcessingInstruction())
			processingInstructions.put(pi.getName(), pi);
	}

	/**
	 * Configures this XML rewriter for the configuration identified by the
	 * given URI.
	 * 
	 * @param uri
	 *            an URI or URI fragment identifying the configuration to load.
	 *            <dl>
	 *            <dt>fragment URI only, e.g., {@code #foo}</dt>
	 *            <dd>Configuration with the ID <em>foo</em> from the current
	 *            import spec.
	 *            <dt>internal URI, e.g., {@code internal:textgrid#aggregation}</dt>
	 *            <dd>One of the predefined configurations.</dd>
	 *            <dt>URL</dt>
	 *            <dl>
	 *            Only theoretically supported yet
	 *            </dl>
	 *            <dt>TextGrid URI</dt>
	 *            <dl>
	 *            not supported yet
	 *            </dl>
	 *            </dl>
	 * 
	 * @throws IllegalArgumentException
	 *             if the given URI cannot be resolved to a valid configuration.
	 * @see #configure(XmlConfiguration)
	 * @see XmlConfiguration
	 */
	public void configure(final URI uri) throws IllegalArgumentException {
		ImportSpec spec;
		if ("internal".equals(uri.getScheme())) {
			final URL resource = getClass().getClassLoader().getResource(
					"specs/" + uri.getSchemeSpecificPart() + ".xml");
			spec = JAXB.unmarshal(resource, ImportSpec.class);
		} else if (uri.getScheme() == null
				|| uri.getSchemeSpecificPart() == null)
			spec = getMapping().toImportSpec();
		else {
			spec = JAXB.unmarshal(uri, ImportSpec.class);
		}
		if (spec != null && spec.getXmlConfiguration() != null) {
			final String fragment = uri.getFragment();
			XmlConfiguration config;
			if (fragment != null) {
				try {
					config = Iterables.find(spec.getXmlConfiguration(),
							new Predicate<XmlConfiguration>() {

								@Override
								public boolean apply(
										final XmlConfiguration input) {
									return fragment.equals(input.getId());
								}
							});
				} catch (final NoSuchElementException e) {
					throw new IllegalArgumentException(MessageFormat.format(
							"Could not find configuration {0} in {1}",
							fragment, uri), e);
				}
			} else if (spec.getXmlConfiguration().size() == 1) {
				config = spec.getXmlConfiguration().get(0);
			} else {
				throw new IllegalArgumentException(
						MessageFormat
								.format("{0} contains more than one configuration, please specify a fragment.",
										uri));
			}
			if (config != null) {
				configure(config);
			}

		}

		// FIXME URI resolvers?
		// TODO extract fragment
	}

	public static List<String> getInternalSpecs() {
		if (internalSpecs == null) {
			internalSpecs = Lists.newLinkedList();
			final InputStream inputStream = ConfigurableXMLRewriter.class
					.getClassLoader().getResourceAsStream("specs/INDEX.txt");
			try {
				final BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream, "UTF-8"));
				String line = reader.readLine();
				while (line != null) {
					if (!line.startsWith("#") && line.trim().length() > 0)
						internalSpecs.add(line.trim());
					line = reader.readLine();
				}
			} catch (final UnsupportedEncodingException e) {
				throw new IllegalStateException(
						"UTF-8 must be available. This is a broken JRE.", e);
			} catch (final IOException e) {
				throw new IllegalStateException(
						"Error reading internal spec file list. This is a bug.",
						e);
			}
		}
		return internalSpecs;
	}

	public static Map<URI, XmlConfiguration> getInternalConfigurations() {
		if (internalConfigurations == null) {
			internalConfigurations = Maps.newLinkedHashMap();
			for (final String internalSpec : getInternalSpecs()) {
				final URL resource = ConfigurableXMLRewriter.class
						.getClassLoader().getResource(
								"specs/" + internalSpec + ".xml");
				final ImportSpec spec = JAXB.unmarshal(resource,
						ImportSpec.class);
				for (final XmlConfiguration configuration : spec
						.getXmlConfiguration()) {
					try {
						internalConfigurations.put(new URI("internal",
								internalSpec, configuration.getId()),
								configuration);
					} catch (final URISyntaxException e) {
						throw new IllegalStateException(e);
					}
				}
			}
		}
		return internalConfigurations;
	}

	protected Replacer getReplacer(final ReplaceMethod method) {
		return replacers.get(method);
	}

	public ConfigurableXMLRewriter(final ImportMapping mapping,
			final boolean export) {
		super(mapping, export);
		replacers = Maps.newEnumMap(ReplaceMethod.class);
		replacers.put(ReplaceMethod.NONE, new Replacer() {

			@Override
			public String apply(final String to) {
				return to;
			}

		});
		replacers.put(ReplaceMethod.FULL, new Replacer() {

			@Override
			public String apply(final String to) {
				final String replacement = rewriteSingleLink(to);
				return replacement == null ? to : replacement;
			}

		});

		replacers.put(ReplaceMethod.TOKEN, new Replacer() {

			@Override
			public String apply(final String to) {
				final StringTokenizer tokenizer = new StringTokenizer(to,
						" \t\n\r\f", true);
				final StringBuilder result = new StringBuilder(to.length());

				while (tokenizer.hasMoreTokens()) {
					final String token = tokenizer.nextToken();
					final String rewritten = rewriteSingleLink(token);
					if (rewritten == null)
						result.append(token);
					else
						result.append(rewritten);
				}
				return result.toString();
			}

		});
	}

	/**
	 * Helper method that correctly applies the configured replacement to the
	 * given content according to te given element.
	 * 
	 * @param startElement
	 *            The name of the element in which the content occurs.
	 * @param content
	 *            (coalesced) character content if the specified element.
	 * @return either content with replacements applied, or content as is.
	 */
	protected String replaceElementContent(final StartElement startElement,
			final String content) {
		final ElementSpec elementSpec = getElementSpec(startElement);
		if (elementSpec == null)
			return content;
		else {
			final Replacer replacer = replacers.get(elementSpec.getMethod());
			if (replacer == null)
				return content;
			else
				return replacer.apply(content);
		}
	}

	/**
	 * Helper method that correctly applies the configured replacement to the
	 * given attribute in the given element according to the rewriter's current
	 * configuration.
	 * 
	 * @param element
	 *            Name of the current element
	 * @param attribute
	 *            Name of the attribute
	 * @return value, either as is or with replacements applied according to the
	 *         configuration.
	 */
	/**
	 * @param element
	 * @param attribute
	 * @return the replaced string
	 */
	protected String replaceAttributeValue(final StartElement element,
			final Attribute attribute) {
		final String value = attribute.getValue();
		try {
			if (mergeMode && XML_ID.equals(attribute.getName()))
				return mergeLinkAdjuster.getMergedID(value);
			final ElementSpec.Attribute attributeSpec = getAttributeSpec(
					element, attribute);
			final Replacer replacer = getReplacer(attributeSpec.getMethod());
			if (replacer != null)
				return replacer.apply(value);
		} catch (final NoSuchElementException e) {
			// no spec -> just return value
		}
		return value;

	}

	private ElementSpec.Attribute getAttributeSpec(final StartElement element,
			final Attribute attribute) throws NoSuchElementException {
		final ElementSpec elementSpec = getElementSpec(element);
		if (elementSpec == null)
			throw new NoSuchElementException("No element spec.");

		try {
			return find(elementSpec.getAttribute(), attribute.getName());
		} catch (final NoSuchElementException e) {
			if (elementSpec != configuration.getAnyElement()
					&& configuration.getAnyElement() != null)
				return find(configuration.getAnyElement().getAttribute(),
						attribute.getName());
			else
				throw e;
		}
	}

	/**
	 * Returns the first {@link ElementSpec} from the
	 * {@linkplain #configure(XmlConfiguration) configured} rewrite
	 * specification that matches the given <var>element</var>.
	 */
	private ElementSpec getElementSpec(final StartElement element) {
		List<ElementSpec> elementSpecs = elements.get(element.getName());
		for (ElementSpec elementSpec : elementSpecs) {
			if (elementSpec.getMode() != null
					&& !elementSpec.getMode().equals(getImportMode()))
				continue; // try next spec
			if (fulfillsRequirements(elementSpec, element))
				return elementSpec;
		}

		// nothing found yet?
		return configuration.getAnyElement();
	}

	/**
	 * Returns <code>true</code> when all {@linkplain ElementSpec.Required
	 * requirements} of the given {@link ElementSpec} are fulfilled by the given
	 * element, or <code>false</code> otherwise.
	 */
	private boolean fulfillsRequirements(ElementSpec elementSpec,
			StartElement element) {
		for (Required required : elementSpec.getRequired()) {
			Attribute attribute = element.getAttributeByName(required
					.getAttribute());
			if (attribute == null)
				return false;
			else if (required.getPattern() != null
					&& !attribute.getValue().matches(required.getPattern()))
				return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p>
	 * This rewriter will rewrite according to the configuration specified using
	 * this class's other methods. See also the class documentation.
	 * </p>
	 * <p>
	 * The rewriter will try to detect the input document's encoding. It will
	 * always serialize to an UTF-8 encoded XML document. It will fail on
	 * non-wellformed documents.
	 * </p>
	 * <p>
	 * This method will create a parser on the input stream and a serializer on
	 * the output and then call {@link #rewrite(XMLEventReader, XMLEventWriter)}
	 * . It will <em>not</em> close the streams you pass in.
	 * </p>
	 */
	@Override
	public void rewrite(final InputStream input, final OutputStream output)
			throws IOException, XMLStreamException {
		// Not compatible with Java > 1.6.0_17:
		// http://linuxtesting.org/upstream-tracker/java/compat_reports/jdk/1.6.0.17_to_1.6.0.18/src_compat_report.html
		// final XMLInputFactory inputFactory = XMLInputFactory.newFactory();
		final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		inputFactory.setProperty(XMLInputFactory.IS_COALESCING, true);
		inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		inputFactory.setProperty(
				XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		final XMLStreamReader streamReader = inputFactory
				.createXMLStreamReader(input);
		String encoding = streamReader.getEncoding();
		if (encoding == null)
			encoding = "UTF-8";
		final XMLEventReader reader = inputFactory
				.createXMLEventReader(streamReader /*
													 * , "UTF-8"
													 */);
		// Same here.
		final XMLEventWriter writer = XMLOutputFactory.newInstance()
				.createXMLEventWriter(output, "UTF-8");
		rewrite(reader, writer);
	}

	public void rewrite(final XMLEventReader reader, final XMLEventWriter writer)
			throws FactoryConfigurationError, XMLStreamException {

		if (logger.isLoggable(Level.FINEST)) {
			StringWriter specWriter = new StringWriter();
			if (getMapping() != null)
				JAXB.marshal(getMapping().toImportSpec(), specWriter);
			logger.finest(MessageFormat
					.format("Starting a rewriting session.\n  * Configuration: {0} ({1})\n  * Base: {2}\n  * Spec:\n{3}",
							getConfiguration().getId(), getConfiguration()
									.getDescription(), getBase(), specWriter
									.toString()));
		}

		final XMLEventFactory eventFactory = XMLEventFactory.newInstance();

		final LinkedList<StartElement> startElements = Lists.newLinkedList();

		while (reader.hasNext()) {
			final XMLEvent event = reader.nextEvent();

			if (event.isStartElement()) {
				final StartElement startElement = event.asStartElement();
				startElements.push(startElement);

				final StartElement newStartElement = eventFactory
						.createStartElement(startElement.getName().getPrefix(),
								startElement.getName().getNamespaceURI(),
								startElement.getName().getLocalPart(), null,
								startElement.getNamespaces(),
								startElement.getNamespaceContext());
				writer.add(newStartElement);

				for (final Iterator<?> attributes = startElement
						.getAttributes(); attributes.hasNext();) {
					final Attribute attribute = (Attribute) attributes.next();
					writer.add(eventFactory.createAttribute(
							attribute.getName(),
							replaceAttributeValue(startElement, attribute)));
				}
			} else if (event.isCharacters()) {
				final Characters characters = event.asCharacters();
				writer.add(eventFactory.createCharacters(replaceElementContent(
						startElements.peek(), characters.getData())));
			} else {
				// TODO handle processing instructions
				writer.add(event);
				if (event.isEndElement())
					startElements.pop();
			}

		}
	}

	protected static <T extends NodeRewriteSpec> T find(
			final Iterable<T> iterable, final QName name)
			throws NoSuchElementException {
		return Iterables.find(iterable, new Predicate<NodeRewriteSpec>() {
			@Override
			public boolean apply(final NodeRewriteSpec input) {
				return name.equals(input.getName());
			}

		});
	}

	/**
	 * After calls to this function the rewriter will record links to
	 * <em>missing references</em>, i.e. to URIs (sans fragment) that have been
	 * identified as links according to the {@linkplain #getConfiguration()
	 * configuration}, that are valid {@link URI}s but that are not contained in
	 * the {@link ImportMapping}.
	 */
	public ConfigurableXMLRewriter recordMissingReferences() {
		missingReferences = Sets.newHashSet();
		return this;
	}

	/**
	 * @return an immutable set containing the URIs of the missing resources
	 *         (sans fragment), or <code>null</code> if nothing has been
	 *         recorded.
	 * @see #recordMissingReferences()
	 */
	public Set<String> getMissingReferences() {
		return ImmutableSet.copyOf(missingReferences);
	}

	/**
	 * Turns <em>merge mode</em> on or off.
	 * 
	 * Merge mode is intended to make xml:ids in documents that should be merged
	 * into a single XML document unique, and adjust links accordingly. In this
	 * mode, the {@linkplain #getMapping() mapping} is ignored
	 */
	public void setMergeMode(final boolean mergeMode) {
		this.mergeMode = mergeMode;
	}

	/**
	 * Configures this rewriter for {@linkplain #setMergeMode(boolean) merge
	 * mode} using the given adjuster to rewrite links.
	 * 
	 * @param adjuster
	 *            An adjuster that rewrites IDs and links.
	 */
	public void setMergeLinkAdjuster(final IMergeLinkAdjuster adjuster) {
		this.mergeLinkAdjuster = adjuster;
		setMergeMode(adjuster != null);
	}

	/**
	 * When using {@linkplain #setMergeMode(boolean) merge mode}, users must
	 * supply an implementation of this interface to adjust links and IDs such
	 * that all IDs in the documents to merge are unique.
	 */
	public interface IMergeLinkAdjuster {

		/**
		 * Rewrite an XML ID. This is called for every xml:id element
		 * encountered in the document to rewrite.
		 * 
		 * @param id
		 *            the ID in the original XML document
		 * @return an ID that will be unique in the merged XML document.
		 */
		public String getMergedID(final String id);

		/**
		 * Rewrite a single link (e.g., {@code textgrid:4711#foo}) for merging.
		 * 
		 * This is called for every single link (according to the rewrite spec)
		 * encountered in the document to rewrite.
		 * 
		 * @param prefix
		 *            The non-fragment part of the link, e.g.:
		 *            {@code textgrid:4711}. May be null or empty.
		 * @param fragment
		 *            The fragment part of the link, e.g.: {@code foo}. May be
		 *            null or empty.
		 * @return The link for the merged document, e.g.: {@code #foo.4711}. If
		 *         this is <code>null</code>, the link will not be modified.
		 */
		public String getMergedLink(final String prefix, final String fragment);
	}

	/**
	 * A default implementation of {@link IMergeLinkAdjuster}, intended for use
	 * with TextGrid URIs. Will rewrite links of the form
	 * {@code textgrid:something#fragment} to {@code #fragment.something}.
	 */
	public static class DefaultMergeLinkAdjuster implements IMergeLinkAdjuster {

		private final String currentDocumentToken;

		public DefaultMergeLinkAdjuster(final URI currentURI) {
			currentDocumentToken = currentURI.getSchemeSpecificPart();
		}

		@Override
		public String getMergedID(final String id) {
			return id + "." + currentDocumentToken;
		}

		@Override
		public String getMergedLink(final String prefix, final String fragment) {
			if (null == prefix || prefix.isEmpty())
				return "#" + getMergedID(fragment);
			else if (null == fragment || fragment.isEmpty())
				return null;
			else
				return "#" + fragment + "."
						+ URI.create(prefix).getSchemeSpecificPart();
		}
	}
}
