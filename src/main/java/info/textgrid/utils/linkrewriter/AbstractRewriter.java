package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportMode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.stream.XMLStreamException;

/**
 * Abstract implementation of {@link ILinkRewriter} that can serve as a base
 * class.
 * 
 * @author vitt
 * 
 */
public abstract class AbstractRewriter implements ILinkRewriter {

	private final ImportMapping mapping;
	private final ImportMode importMode;

	
	protected AbstractRewriter(final ImportMapping mapping, final ImportMode importMode) {
		super();
		this.mapping = mapping;
		this.importMode = importMode;
	}
	
	@Deprecated
	protected AbstractRewriter(final ImportMapping mapping, final boolean export) {
		this(mapping, export? ImportMode.EXPORT : ImportMode.IMPORT);
	}

	@Override
	public ImportMapping getMapping() {
		return mapping;
	}

	@Override
	public boolean isExport() {
		return ImportMode.EXPORT.equals(importMode);
	}
	
	@Override
	public ImportMode getImportMode() {
		return importMode;
	}

	@Override
	public abstract void rewrite(final InputStream input, final OutputStream output) throws IOException, XMLStreamException;

}