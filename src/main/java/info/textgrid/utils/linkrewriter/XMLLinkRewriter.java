package info.textgrid.utils.linkrewriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLLinkRewriter extends AbstractRewriter implements ILinkRewriter {

	private final List<TokenReplacement> replacements;

	protected XMLLinkRewriter(final ImportMapping mapping, final boolean export) {
		super(mapping, export);
		replacements = AbstractReplacement.create(TokenReplacement.class, mapping, export);
	}

	@Override
	public void rewrite(final InputStream input, final OutputStream output) throws IOException, XMLStreamException {
		try {
			// Not compatible with Java > 1.6.0_17:
			// http://linuxtesting.org/upstream-tracker/java/compat_reports/jdk/1.6.0.17_to_1.6.0.18/src_compat_report.html
			// final XMLInputFactory inputFactory =
			// XMLInputFactory.newFactory();
			final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			inputFactory.setProperty(XMLInputFactory.IS_COALESCING, true);
			final XMLEventReader reader = inputFactory.createXMLEventReader(input);
			// Same here.
			final XMLEventWriter writer = XMLOutputFactory.newInstance().createXMLEventWriter(output);
			final XMLEventFactory eventFactory = XMLEventFactory.newInstance();

			while (reader.hasNext()) {
				final XMLEvent event = reader.nextEvent();

				if (event.isStartElement()) {
					final StartElement startElement = event.asStartElement();

					final StartElement newStartElement = eventFactory.createStartElement(startElement.getName().getPrefix(),
							startElement.getName().getNamespaceURI(), startElement.getName().getLocalPart(), null, null,
							startElement.getNamespaceContext());
					writer.add(newStartElement);

					// writer.add(event); // FIXME Duplicates attributes
					for (final Iterator<?> attributes = startElement.getAttributes(); attributes.hasNext();) {
						final Attribute attribute = (Attribute) attributes.next();
						final String value = attribute.getValue();
						final String result = AbstractReplacement.apply(replacements, value);
						writer.add(eventFactory.createAttribute(attribute.getName(), result));
					}
				} else if (event.isCharacters()) {
					final Characters characters = event.asCharacters();
					final String data = characters.getData();
					final String result = AbstractReplacement.apply(replacements, data);
					writer.add(eventFactory.createCharacters(result));
				} else {
					writer.add(event);
				}
			}
		} catch (final FactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
