package info.textgrid.utils.export.filenames;

import static junit.framework.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import junit.framework.Assert;

import org.junit.Test;

public class ConfigurableFilenamePolicyTest extends AbstractFilenamePolicyBase {

	@Test
	public void testConfigurableFilename() throws URISyntaxException {
		final ConfigurableFilenamePolicy policy = ConfigurableFilenamePolicy.builder(
				"{title}-{uri}.{ext}").build();
		final URI filename = policy.getFilename(xample1);
		System.out.println(filename);
		Assert.assertEquals(URI.create("Test_Title-textgrid_xample1.xml"),
				filename);
	}

	@Test
	public void testNestedPolicies() {
		final ConfigurableFilenamePolicy parentPolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{title}")
				.isParent()
				.build();
		final ConfigurableFilenamePolicy filenamePolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{author|fallback}-{title}.{uri}.{ext}")
				.subPolicy("parent", parentPolicy)
				.build();
		final ConfigurableFilenamePolicy metaPolicy = ConfigurableFilenamePolicy
				.builder(".meta/{filename}.meta")
                .subPolicy("parent", parentPolicy)
                .subPolicy("filename", filenamePolicy)
                .build();

		Assert.assertEquals(URI.create("Ueber-Edition/Sub/Maennchen,_Muster-Test_Title.textgrid_xample1.xml"), filenamePolicy.getFilename(xample1));
		Assert.assertEquals(URI.create(".meta/Ueber-Edition/Sub/Maennchen,_Muster-Test_Title.textgrid_xample1.xml.meta"), metaPolicy.getFilename(xample1));
	}

	@Test
	public void testBase() {
		final ConfigurableFilenamePolicy parentPolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{title}")
				.isParent()
				.build();
		final ConfigurableFilenamePolicy filenamePolicy = ConfigurableFilenamePolicy
				.builder("{parent|/}{author}-{title}.{uri}.{ext}")
				.subPolicy("parent", parentPolicy)
				.build();

		assertEquals(URI.create("Ueber-Edition/Sub/"), filenamePolicy.getBase(xample1).get());
		assertEquals(URI.create("Ueber-Edition/"), filenamePolicy.getBase(xample1.getParent().get()).get());
	}

	@Test
	public void testUniquePolicies() {
		final ConfigurableFilenamePolicy policy = ConfigurableFilenamePolicy.builder("{title}*.{ext}").build();
		assertEquals(URI.create("Test_Title.xml"), policy.getFilename(xample1));
		assertEquals(URI.create("Test_Title.1.xml"), policy.getFilename(xample2));
		assertEquals(URI.create("Test_Title.xml"), policy.getFilename(xample1));
		assertEquals(URI.create("Test_Title.1.xml"), policy.getFilename(xample2));
	}

	@Test
	public void testOptions() {
		final ConfigurableFilenamePolicy policy = ConfigurableFilenamePolicy
			.builder("{author|fallback|10}.{title|5}*.{ext}").build();
		assertEquals(URI.create("Maennchen,.Test_.xml"), policy.getFilename(xample1));
	}
	
	@Test
	public void testTransformedExt() {
		xample1.setTransformedFormat("text/plain");
		final ConfigurableFilenamePolicy policy = ConfigurableFilenamePolicy.builder(
				"{title}-{uri}.{ext}").build();
		final URI filename = policy.getFilename(xample1);
		System.out.println(filename);
		Assert.assertEquals(URI.create("Test_Title-textgrid_xample1.txt"),
				filename);
	}

}
