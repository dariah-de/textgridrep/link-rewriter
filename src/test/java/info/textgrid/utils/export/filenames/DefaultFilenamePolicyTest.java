package info.textgrid.utils.export.filenames;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.net.URI;
import java.util.regex.Pattern;

import org.junit.Test;

import com.google.common.base.Optional;

public class DefaultFilenamePolicyTest extends AbstractFilenamePolicyBase {

	@Test
	public void testTranslate() {
		final Pattern pattern = Pattern.compile("[a-zA-Z0-9.!~_-]+");
		final String[] filenames = {
				"Märchen",
				"MÄRCHEN",
				"Lustige Märchen",
				"3½ Luſtige Märchen",
				"ἑλληνικὴ γλῶσσα",
				"اللغة العربية",
				"國語羅馬字",
				"देवनागरी",
		};
		for (final String filename : filenames) {
			final String safename = policy.translate(filename);
			System.out.println(filename + " -> " + safename);
			assertTrue(safename + " (from " + filename + ") is not safe", pattern.matcher(safename).matches());
		}
	}

	@Test
	public void testGetFilenameforMetadata() {
		assertEquals("Test_Title.xample1.xml", policy.getFilename(xample1.getMetadata(), false));
		assertEquals("Ueber-Edition.super1.edition", policy.getFilename(super1.getMetadata(), false));
		assertEquals("Ueber-Edition.super1/", policy.getFilename(super1.getMetadata(), true));
	}

	@Test
	public void testGetFilenameforEntry() {
		assertEquals(URI.create("Ueber-Edition.super1/Sub.sub/Test_Title.xample1.xml"), policy.getFilename(xample1));
	}

	@Test
	public void testGetBase() {
		assertEquals(Optional.of(URI.create("Ueber-Edition.super1/Sub.sub/")), policy.getBase(xample1));
	}

}
