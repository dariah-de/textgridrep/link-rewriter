package info.textgrid.utils.export.filenames;

import info.textgrid.utils.export.filenames.FileExtensionMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;

public class FileExtensionMapTest {

	private static final String APPLICATION_XHTML_XML = "application/xhtml+xml";
	private FileExtensionMap ext;

	@Before
	public void setUp() throws Exception {
		ext = FileExtensionMap.getInstance();
	}

	@Test
	public void testGetExtensions() {
		final ImmutableCollection<String> xhtml = ext
				.getExtensions(APPLICATION_XHTML_XML);
		Assert.assertFalse("No extensions for xhtml?", xhtml.isEmpty());
		Assert.assertEquals("Wrong extensions for XHTML",
				ImmutableList.of("xhtml", "xht"), xhtml);
	}
	
	@Test
	public void testTextGridTypes() {
		Assert.assertEquals("aggregation", ext.getFirstExtension("text/tg.aggregation+xml").orNull());
	}

	@Test
	public void testGetFirstExtension() {
		Assert.assertTrue("No first extension for XHTML", ext
				.getFirstExtension(APPLICATION_XHTML_XML).isPresent());
		Assert.assertEquals("xhtml",
				ext.getFirstExtension(APPLICATION_XHTML_XML).get());
	}

	@Test
	public void testAddExtension() {
		Assert.assertEquals("test.xhtml",
				ext.addExtension("test", APPLICATION_XHTML_XML));
	}

}
