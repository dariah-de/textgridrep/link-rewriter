package info.textgrid.utils.export.filenames;

import info.textgrid.namespaces.metadata.agent._2010.AgentRoleType;
import info.textgrid.namespaces.metadata.agent._2010.AgentType;
import info.textgrid.namespaces.metadata.core._2010.EditionType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.GenericType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.metadata.core._2010.WorkType;
import info.textgrid.utils.export.aggregations.Aggregation;
import info.textgrid.utils.export.aggregations.AggregationEntry;
import info.textgrid.utils.export.aggregations.ITransformableAggregationEntry;

import org.junit.Before;

public class AbstractFilenamePolicyBase {

	private static final String AGGREGATION_XML = "text/tg.aggregation+xml";
	private static final String EDITION_XML = "text/tg.edition+tg.aggregation+xml";
	private static final String WORK_XML = "text/tg.work+xml";
	private static final String XML = "text/xml";
	protected Aggregation super1;
	private Aggregation sub;
	protected ITransformableAggregationEntry xample1;
	protected DefaultFilenamePolicy policy;
	protected DefaultMetaFilenamePolicy metaPolicy;
	protected AggregationEntry work1;
	protected ITransformableAggregationEntry xample2;

	public AbstractFilenamePolicyBase() {
		super();
	}

	@Before
	public void setUp() throws Exception {
		final ObjectType super1MD = createSimpleMetadata("Über-Edition", EDITION_XML, "textgrid:super1");
		final ObjectType workMD = createSimpleMetadata("Ein Werk", WORK_XML, "textgrid:work");
		final WorkType work = new WorkType();
		workMD.setWork(work);
		final AgentType agent = new AgentType();
		agent.setRole(AgentRoleType.EDITOR);
		agent.setValue("Männchen, Muster");
		work.getAgent().add(agent);
		final EditionType edition = new EditionType();
		edition.setIsEditionOf("textgrid:work");
		super1MD.setEdition(edition);

		final ObjectType subMD = createSimpleMetadata("Sub", AGGREGATION_XML, "textgrid:sub");
		final ObjectType xample1MD = createSimpleMetadata("Test Title", XML, "textgrid:xample1");
		final ObjectType xample2MD = createSimpleMetadata("Test Title", XML, "textgrid:xample2");

		super1 = new Aggregation(super1MD);
		work1 = new AggregationEntry(workMD, super1);
		super1.setWork(work1);

		sub = new Aggregation(subMD, super1);
		xample1 = new AggregationEntry(xample1MD, sub);
		xample2 = new AggregationEntry(xample2MD, sub);

		policy = new DefaultFilenamePolicy();
		metaPolicy = new DefaultMetaFilenamePolicy(policy);

	}

	private ObjectType createSimpleMetadata(final String title, final String format, final String uri) {
		final ObjectType metadata = new ObjectType();
		final GenericType generic = new GenericType();
		final GeneratedType generated = new GeneratedType();
		final ProvidedType provided = new ProvidedType();
		generic.setGenerated(generated);;
		generic.setProvided(provided);
		provided.getTitle().add(title);
		provided.setFormat(format);
		final TextgridUri textgridUri = new TextgridUri();
		textgridUri.setValue(uri);
		generated.setTextgridUri(textgridUri);
		metadata.setGeneric(generic);

		return metadata;
	}

}