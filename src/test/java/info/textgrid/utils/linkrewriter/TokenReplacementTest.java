package info.textgrid.utils.linkrewriter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TokenReplacementTest {

	@Test
	public void testApply() {

		final TokenReplacement replacement = new TokenReplacement("foo.xml", "textgrid:foo");
		assertEquals("textgrid:foo", replacement.apply("foo.xml"));
		assertEquals("Ein Test mit textgrid:foo drin", replacement.apply("Ein Test mit foo.xml drin"));

	}

}
