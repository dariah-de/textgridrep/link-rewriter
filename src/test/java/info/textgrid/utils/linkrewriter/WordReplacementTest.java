package info.textgrid.utils.linkrewriter;

import static org.junit.Assert.assertEquals;
import info.textgrid._import.RewriteMethod;

import java.util.List;

import org.junit.Test;

public class WordReplacementTest {

	@Test
	public void testApply() {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:foo", "foo.xml", "foo.xml.meta", RewriteMethod.TEXT);
		final SimpleTextRewriter importRewriter = new SimpleTextRewriter(mapping, false);

		final List<? extends AbstractReplacement> replacements = importRewriter.prepareReplacements();
		final AbstractReplacement replacement = replacements.get(0);

		assertEquals("Siehe auch textgrid:foo, denn in textgrid:foo steht die Wahrheit, in blafop.xml nicht.",
				replacement.apply("Siehe auch foo.xml, denn in foo.xml steht die Wahrheit, in blafop.xml nicht."));
	}
}
