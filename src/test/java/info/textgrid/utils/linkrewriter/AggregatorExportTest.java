package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

import com.google.common.io.InputSupplier;
import com.google.common.io.Resources;

public class AggregatorExportTest {

	@Test
	public void test() throws IOException, XMLStreamException {
		final ImportMapping mapping = new ImportMapping();
		final ImportObject io = new ImportObject();
		io.setTextgridUri("textgrid:jkj8.0");
		io.setLocalData("jkj8.0.jpg");
		mapping.add(io);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(mapping, true);
		rewriter.configure(URI.create("internal:html#html"));

		final InputSupplier<InputStream> inputSupplier = Resources.newInputStreamSupplier(Resources.getResource("tg4.jmzc.0.html"));
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		rewriter.rewrite(inputSupplier.getInput(), outputStream);

	}

}
