package info.textgrid.utils.linkrewriter;

import static info.textgrid.utils.linkrewriter.SimpleTextRewriterTest.*;
import static org.junit.Assert.*;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.ReplaceMethod;
import info.textgrid._import.RevisionPolicy;
import info.textgrid._import.RevisionPolicyType;
import info.textgrid._import.RewriteMethod;
import info.textgrid._import.XmlConfiguration;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter.Replacer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Set;

import jakarta.xml.bind.JAXB;
import javax.xml.stream.XMLStreamException;

import org.custommonkey.xmlunit.SimpleNamespaceContext;
import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.exceptions.XpathException;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;
import com.google.common.io.FileBackedOutputStream;
import com.google.common.io.Resources;

public class ConfigurableXMLRewriterTest extends XMLTestCase {
	private static final String	in			= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
													+ "<test norewrite=\"foo.xml\">\n"
													+ "	Dies ist ein <a href=\"foo.xml\">Link auf foo.xml</a>, hier auch mit <a href=\"foo.xml#fragment\">Fragment.</a>\n"
													+ "	Eine URL: <url>foo.xml</url>, und zwei: <linkgroup refs=\"foo.xml bar/test.png\"/>.\n"
													+ "</test>";

	private static final String	out			= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
													+ "<test norewrite=\"foo.xml\">\n"
													+ "	Dies ist ein <a href=\"textgrid:foo\">Link auf foo.xml</a>, hier auch mit <a href=\"textgrid:foo#fragment\">Fragment.</a>\n"
													+ "	Eine URL: <url>textgrid:foo</url>, und zwei: <linkgroup refs=\"textgrid:foo textgrid:bar\"></linkgroup>.\n"
													+ "</test>";

	private static final String	outOnlyFull	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
													+ "<test norewrite=\"foo.xml\">\n"
													+ "	Dies ist ein <a href=\"textgrid:foo\">Link auf foo.xml</a>, hier auch mit <a href=\"textgrid:foo#fragment\">Fragment.</a>\n"
													+ "	Eine URL: <url>textgrid:foo</url>, und zwei: <linkgroup refs=\"foo.xml bar/test.png\"></linkgroup>.\n"
													+ "</test>";

	private static final String	spec1		= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
													+ "<i:xmlConfiguration xml:id=\"test1\" description=\"Test 1\"\n"
													+ "	xmlns:i=\"http://textgrid.info/import\">\n"
													+ "	<i:element name=\"url\" method=\"full\" />\n"
													+ "	<i:element name=\"a\" method=\"none\">\n"
													+ "		<i:attribute name=\"href\" method=\"full\" />\n"
													+ "	</i:element>\n"
													+ "	<i:element name=\"linkgroup\" method=\"none\">\n"
													+ "		<i:attribute name=\"refs\" method=\"token\" />\n"
													+ "	</i:element>\n"
													+ "</i:xmlConfiguration>";

	@Test
	public void testNoneReplacer() {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.NONE);
		assertEquals("foo.xml", replacer.apply("foo.xml"));
		assertEquals(" foo.xml", replacer.apply(" foo.xml"));
		assertEquals("bla blubb", replacer.apply("bla blubb"));
	}

	private ConfigurableXMLRewriter setupRewriter() {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:foo", "foo.xml", null, RewriteMethod.XML);
		mapping.add("textgrid:bar", "bar/test.png", null, null);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, false);
		return rewriter;
	}

	@Test
	public void testFullReplacer() {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.FULL);
		assertEquals("textgrid:foo", replacer.apply("foo.xml"));
		assertEquals("textgrid:foo#somewhere",
				replacer.apply("foo.xml#somewhere"));
		assertEquals("textgrid:foo", replacer.apply(" foo.xml"));
		assertEquals("bar foo.xml", replacer.apply("bar foo.xml"));
		assertEquals("bar ", replacer.apply("bar "));
	}

	@Test
	public void testAddConfiguration() {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final XmlConfiguration configuration = JAXB.unmarshal(
				stringToStream(spec1), XmlConfiguration.class);
		assertEquals(3, configuration.getElement().size());

		rewriter.configure(configuration);
		// TODO
	}

	@Test
	public void testRewrite() throws IOException, XMLStreamException {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final XmlConfiguration configuration = JAXB.unmarshal(
				stringToStream(spec1), XmlConfiguration.class);
		rewriter.configure(configuration);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(in), baos);
		final String result = baos.toString("UTF-8");
		// System.out.println(result);
		assertEquals(out, result);
	}
	
	@Test
	public void testIdno() throws IOException, XMLStreamException, XpathException, SAXException {
		ImportMapping mapping = new ImportMapping();
		mapping.add(URI.create("textgrid:idnotest"), URI.create("idnoTest.xml"), null, RewriteMethod.XML);
		mapping.add(URI.create("textgrid:target"), URI.create("target.xml"), null, RewriteMethod.XML);
		ConfigurableXMLRewriter importRewriter = new ConfigurableXMLRewriter(mapping, false);
		importRewriter.configure(URI.create("internal:tei#tei"));
		
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("idnoTest.xml");
		FileBackedOutputStream outputStream = new FileBackedOutputStream(1024*1024);
		importRewriter.rewrite(inputStream, outputStream);
		
		XMLUnit.setXpathNamespaceContext(new SimpleNamespaceContext(ImmutableMap.of("tei", "http://www.tei-c.org/ns/1.0")));
		assertXpathEvaluatesTo("textgrid:idnotest", "//tei:idno[@type='textgrid']", new InputSource(outputStream.asByteSource().openStream()));
		assertXpathEvaluatesTo("idnoTest.xml", "//tei:idno[@type='internal']", new InputSource(outputStream.asByteSource().openStream()));
		
		// now export it again
		ConfigurableXMLRewriter exportRewriter = new ConfigurableXMLRewriter(mapping, true);
		exportRewriter.configure(URI.create("internal:tei#tei"));
		FileBackedOutputStream exportedStream = new FileBackedOutputStream(1024*1024);
		exportRewriter.rewrite(outputStream.asByteSource().openStream(), exportedStream);
		
		// there should be _no_ rewriting of idno on export
		assertXpathEvaluatesTo("textgrid:idnotest", "//tei:idno[@type='textgrid']", new InputSource(exportedStream.asByteSource().openStream()));
	}

	@Test
	public void testRecordMissing() throws IOException, XMLStreamException {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:foo", "foo.xml", null, RewriteMethod.XML);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, false);
		rewriter.recordMissingReferences();
		final XmlConfiguration configuration = JAXB.unmarshal(
				stringToStream(spec1), XmlConfiguration.class);
		rewriter.configure(configuration);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(in), baos);
		final Set<String> missingReferences = rewriter.getMissingReferences();
		System.out.println(missingReferences);
		assertEquals(1, missingReferences.size());
		assertEquals("bar/test.png", missingReferences.iterator().next());
	}

	@Test
	public void testRewriteNamespaces() throws IOException, XMLStreamException {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final XmlConfiguration configuration = JAXB.unmarshal(
				stringToStream(spec1), XmlConfiguration.class);
		rewriter.configure(configuration);
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(in_ns), baos);
		final String result = baos.toString("UTF-8");
		// System.out.println(result);
		assertEquals(out_ns, result);
	}

	@Test
	public void testConfigureInternal() {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		rewriter.configure(URI.create("internal:textgrid#metadata"));
		assertEquals("TextGrid Metadata", rewriter.getConfiguration()
				.getDescription());
	}

	@Test
	public void testConfigureFragment() {
		final ConfigurableXMLRewriter rewriter = setupRewriter();
		final XmlConfiguration configuration = JAXB.unmarshal(
				stringToStream(spec1), XmlConfiguration.class);
		final ImportSpec importSpec = rewriter.getMapping().toImportSpec();
		importSpec.getXmlConfiguration().add(configuration);

		final ConfigurableXMLRewriter rewriter2 = new ConfigurableXMLRewriter(
				new ImportMapping(importSpec), false);

		rewriter2.configure(URI.create("#test1"));
		assertEquals("Test 1", rewriter2.getConfiguration().getDescription());
	}

	private static final String	in_ns	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
												+ "<test norewrite=\"foo.xml\">\n"
												+ "	Dies ist ein <a href=\"foo.xml\">Link auf foo.xml</a>, hier auch mit <a href=\"foo.xml#fragment\">Fragment.</a>\n"
												+ "	Eine URL: <url>foo.xml</url>, <em xmlns=\"http://www.w3.org/1999/xhtml\">und</em> zwei: <linkgroup refs=\"foo.xml bar/test.png\"/>.\n"
												+ "</test>";

	private static final String	out_ns	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<test norewrite=\"foo.xml\">\n"
												+ "	Dies ist ein <a href=\"textgrid:foo\">Link auf foo.xml</a>, hier auch mit <a href=\"textgrid:foo#fragment\">Fragment.</a>\n"
												+ "	Eine URL: <url>textgrid:foo</url>, <em xmlns=\"http://www.w3.org/1999/xhtml\">und</em> zwei: <linkgroup refs=\"textgrid:foo textgrid:bar\"></linkgroup>.\n"
												+ "</test>";

	@Test
	public void testInternalSpecs() {
		final List<String> specs = ConfigurableXMLRewriter.getInternalSpecs();
		assertTrue(specs.contains("textgrid"));
		assertTrue(!specs.contains("doesnotexist"));
	}

	@Test
	public void testInternalConfigs() {
		final XmlConfiguration configuration = ConfigurableXMLRewriter
				.getInternalConfigurations().get(
						URI.create("internal:textgrid#aggregation"));
		assertTrue(configuration != null);
		assertTrue(configuration.getDescription().contains("Aggregation"));
	}

	private static final String	in_aggr		= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
													+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
													+ " xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
													+ "        <rdf:Description rdf:about=\"textgrid:about.0\">\n"
													+ "                <ore:aggregates>textgrid:uri1.0</ore:aggregates>\n"
													+ "                <ore:aggregates>textgrid:uri2.0</ore:aggregates>\n"
													+ "        </rdf:Description>\n"
													+ "</rdf:RDF>";

	private static final String	out_aggr	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
													+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
													+ " xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
													+ "        <rdf:Description rdf:about=\"about.aggregation\">\n"
													+ "                <ore:aggregates>object1.xml</ore:aggregates>\n"
													+ "                <ore:aggregates>object2.xml</ore:aggregates>\n"
													+ "        </rdf:Description>\n"
													+ "</rdf:RDF>";

	@Test
	public void testAggregation() throws IOException, XMLStreamException {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:about.0", "about.aggregation", null,
				RewriteMethod.XML);
		mapping.add("textgrid:uri1.0", "object1.xml", null, RewriteMethod.XML);
		mapping.add("textgrid:uri2.0", "object2.xml", null, RewriteMethod.XML);

		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, true);
		rewriter.configure(URI.create("internal:textgrid#aggregation"));

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(in_aggr), baos);
		final String result = baos.toString("UTF-8");
		assertEquals(out_aggr, result);
	}

	@Test
	public void testAggregationReverseAndSpecialChars() throws IOException,
			XMLStreamException {
		final String a1 = "./about.aggregation";
		final String a2 = "./object 1öäü.xml";
		final String a3 = "./folder/noch'n'folder/ob je ct2§$%.xml";
		final String a4 = new File(a1).getCanonicalFile().toURI().toString();
		final String a5 = new File(a2).getCanonicalFile().toURI().toString();
		final String a6 = new File(a3).getCanonicalFile().toURI().toString();

		// Set base path.
		final URI basePath = new File(a1).getParentFile().getCanonicalFile()
				.toURI();
		System.out.println("base path: " + basePath);

		final URI l4 = URI.create("./"
				+ a4.substring(basePath.toASCIIString().length()));
		final URI l5 = URI.create("./"
				+ a5.substring(basePath.toASCIIString().length()));
		final URI l6 = URI.create("./"
				+ a6.substring(basePath.toASCIIString().length()));

		System.out.println();
		System.out.println("relative uris from aggregation file entries:");
		System.out.println("l4: " + l4);
		System.out.println("l5: " + l5);
		System.out.println("l6: " + l6);

		final String inAggr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\""
				+ " xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
				+ "        <rdf:Description rdf:about=\"" + l4.toString()
				+ "\">\n" + "                <ore:aggregates rdf:resource=\""
				+ l5.toString() + "\" />\n"
				+ "                <ore:aggregates rdf:resource=\""
				+ l6.toString() + "\" />\n" + "        </rdf:Description>\n"
				+ "</rdf:RDF>";

		final String specialAggr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
				+ "        <rdf:Description rdf:about=\"textgrid:about.0\">\n"
				+ "                <ore:aggregates rdf:resource=\"textgrid:uri1.0\"></ore:aggregates>\n"
				+ "                <ore:aggregates rdf:resource=\"textgrid:uri2.0\"></ore:aggregates>\n"
				+ "        </rdf:Description>\n" + "</rdf:RDF>";

		// Get URI from files.
		final URI u1 = new File(a1).getCanonicalFile().toURI();
		final URI u2 = new File(a2).getCanonicalFile().toURI();
		final URI u3 = new File(a3).getCanonicalFile().toURI();

		System.out.println();
		System.out.println("uris from strings as absoulte path:");
		System.out.println("u1: " + u1);
		System.out.println("u2: " + u2);
		System.out.println("u3: " + u3);

		// Put URI parts in rewrite mapping.
		final URI t1 = URI.create("textgrid:about.0");
		final URI t2 = URI.create("textgrid:uri1.0");
		final URI t3 = URI.create("textgrid:uri2.0");

		final ImportMapping mapping = new ImportMapping();
		mapping.add(t1, u1, null, RewriteMethod.XML);
		mapping.add(t2, u2, null, RewriteMethod.XML);
		mapping.add(t3, u3, null, RewriteMethod.XML);

		System.out.println();
		System.out.println("rewrite map:");
		System.out.println(mapping.getImportObjectForLocalURI(u1)
				.getTextgridUri()
				+ " = "
				+ mapping.getImportObjectForTextGridURI(t1).getLocalData());
		System.out.println(mapping.getImportObjectForLocalURI(u2)
				.getTextgridUri()
				+ " = "
				+ mapping.getImportObjectForTextGridURI(t2).getLocalData());
		System.out.println(mapping.getImportObjectForLocalURI(u3)
				.getTextgridUri()
				+ " = "
				+ mapping.getImportObjectForTextGridURI(t3).getLocalData());

		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, false);
		rewriter.configure(URI.create("internal:textgrid#aggregation"));
		rewriter.setBase(basePath);

		System.out.println();
		System.out.println("inAggr:");
		System.out.println(inAggr);

		System.out.println();
		System.out.println("specialAggr:");
		System.out.println(specialAggr);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(inAggr), baos);
		final String result = baos.toString("UTF-8");

		System.out.println();
		System.out.println("result:");
		System.out.println(result);

		assertEquals(specialAggr, result);
	}

	@Test
	public void testReplacerRelativize() {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:about.0", "about.aggregation", null,
				RewriteMethod.XML);
		mapping.add("textgrid:uri1.0", "about/object1.xml", null,
				RewriteMethod.XML);
		mapping.add("textgrid:uri2.0", "about/object2.xml", null,
				RewriteMethod.XML);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, true);
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.FULL);
		assertEquals("about.aggregation", replacer.apply("textgrid:about.0"));
		rewriter.setBase(URI.create("about/"));
		assertEquals("object1.xml", replacer.apply("textgrid:uri1.0"));
	}

	@Test
	public void testReplacerResolve() {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:about.0", "about.aggregation", null,
				RewriteMethod.XML);
		mapping.add("textgrid:uri1.0", "about/object1.xml", null,
				RewriteMethod.XML);
		mapping.add("textgrid:uri2.0", "about/object2.xml", null,
				RewriteMethod.XML);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, false);
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.FULL);
		assertEquals("textgrid:about.0", replacer.apply("about.aggregation"));
		rewriter.setBase(URI.create("about/"));
		assertEquals("textgrid:uri1.0", replacer.apply("object1.xml"));
	}

	@Test
	public void testReplacerRevisionExport() {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:uri1", "object1.xml", null, RewriteMethod.XML);
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, true);
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.FULL);
		assertEquals("object1.xml", replacer.apply("textgrid:uri1"));
		assertEquals("object1.xml", replacer.apply("textgrid:uri1.0"));
	}

	@Test
	public void testReplacerRevisionImport() {
		final ImportMapping mapping0 = new ImportMapping();
		mapping0.add("textgrid:about.0", "about.aggregation", null,
				RewriteMethod.XML);
		mapping0.add("textgrid:uri1.0", "about/object1.xml", null,
				RewriteMethod.XML);
		mapping0.add("textgrid:uri2.0", "about/object2.xml", null,
				RewriteMethod.XML);
		final ImportSpec importSpec = mapping0.toImportSpec();
		final RevisionPolicy revisionPolicy = new RevisionPolicy();
		revisionPolicy.setImport(RevisionPolicyType.LATEST);
		importSpec.setRevisionPolicy(revisionPolicy);
		final ImportMapping mapping = new ImportMapping(importSpec);

		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				mapping, false);
		final Replacer replacer = rewriter.getReplacer(ReplaceMethod.FULL);
		assertEquals("textgrid:about", replacer.apply("about.aggregation"));
		rewriter.setBase(URI.create("about/"));
		assertEquals("textgrid:uri1", replacer.apply("object1.xml"));
	}

	@Test
	public void testMergeReplacement() throws IOException, XMLStreamException {
		final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(
				null, true);
		rewriter.configure(URI.create("internal:tei#tei"));
		rewriter.setMergeLinkAdjuster(new ConfigurableXMLRewriter.DefaultMergeLinkAdjuster(
				URI.create("textgrid:0815")));
		final InputStream inputStream = getClass().getResourceAsStream(
				"/teimerge.xml");
		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		rewriter.rewrite(inputStream, outputStream);
		final String output = outputStream.toString("UTF-8");
		final String expected = CharStreams.toString(Resources
				.newReaderSupplier(Resources.getResource("teimerged.xml"),
						Charsets.UTF_8));
		assertEquals(expected, output);
	}

}
