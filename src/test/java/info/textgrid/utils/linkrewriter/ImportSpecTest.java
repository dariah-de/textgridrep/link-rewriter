package info.textgrid.utils.linkrewriter;

import static org.junit.Assert.assertFalse;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.XmlConfiguration;

import java.net.URL;
import java.util.List;

import jakarta.xml.bind.JAXB;

import org.junit.Test;

public class ImportSpecTest {

	@Test
	public void testLoadSpec() {
		final URL resource = getClass().getClassLoader().getResource("specs/html.xml");
		final ImportSpec spec = JAXB.unmarshal(resource, ImportSpec.class);
		final List<XmlConfiguration> configurations = spec.getXmlConfiguration();
		assertFalse(configurations.isEmpty());
		final XmlConfiguration configuration = configurations.get(0);
		System.out.println(configuration);
	}

}
