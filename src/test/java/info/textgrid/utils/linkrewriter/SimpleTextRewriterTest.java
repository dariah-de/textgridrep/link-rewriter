package info.textgrid.utils.linkrewriter;

import static org.junit.Assert.assertEquals;
import info.textgrid._import.RewriteMethod;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

public class SimpleTextRewriterTest {

	final String local = "This is a file referencing foo.xml as well as foo/bar.png, and also foo.xml#id and foo.xml#xpointer(foo[1]).";
	final String textgrid = "This is a file referencing textgrid:foo as well as textgrid:bar, and also textgrid:foo#id and textgrid:foo#xpointer(foo[1]).";

	/**
	 * prepares the test rewriter.
	 * 
	 * @param export
	 *            whether this is an export rewriter
	 */
	private SimpleTextRewriter createRewriter(final boolean export) {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:foo", "foo.xml", null, RewriteMethod.TEXT);
		mapping.add("textgrid:bar", "foo/bar.png", "foo/bar.meta", null);

		final SimpleTextRewriter rewriter = new SimpleTextRewriter(mapping, export);
		return rewriter;
	}

	@Test
	public void testPrepareReplacements() {
		final SimpleTextRewriter rewriter = createRewriter(false);
		final List<? extends AbstractReplacement> replacements = rewriter.prepareReplacements();

		assertEquals(
				"[WordReplacement [from=foo.xml, to=textgrid:foo, pattern=\\Qfoo.xml\\E], WordReplacement [from=foo/bar.png, to=textgrid:bar, pattern=\\Qfoo/bar.png\\E]]",
				replacements.toString());

		final SimpleTextRewriter exportRewriter = createRewriter(true);
		final List<? extends AbstractReplacement> exportReplacements = exportRewriter.prepareReplacements();

		assertEquals(
				"[WordReplacement [from=textgrid:foo, to=foo.xml, pattern=\\Qtextgrid:foo\\E], WordReplacement [from=textgrid:bar, to=foo/bar.png, pattern=\\Qtextgrid:bar\\E]]",
				exportReplacements.toString());

	}

	@Test
	public void testRewriteImport() throws IOException, XMLStreamException {
		final ByteArrayInputStream input = new ByteArrayInputStream(local.getBytes(Charset.forName("UTF-8")));
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		final ILinkRewriter importRewriter = createRewriter(false);
		importRewriter.rewrite(input, output);
		output.close();
		final String result = output.toString("UTF-8");
		assertEquals(textgrid, result);
	}

	@Test
	public void testRewriteExport() throws IOException, XMLStreamException {
		final ByteArrayInputStream input = stringToStream(textgrid);
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		final ILinkRewriter exportRewriter = createRewriter(true);
		exportRewriter.rewrite(input, output);
		output.close();
		final String result = output.toString("UTF-8");
		assertEquals(local, result);
	}

	public static ByteArrayInputStream stringToStream(final String textgrid) {
		return new ByteArrayInputStream(textgrid.getBytes(Charset.forName("UTF-8")));
	}

}
