package info.textgrid.utils.linkrewriter;

import static info.textgrid.utils.linkrewriter.SimpleTextRewriterTest.stringToStream;
import static org.junit.Assert.assertEquals;
import info.textgrid._import.RewriteMethod;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

public class XMLLinkRewriterTest {

	private static final String in = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<test>\n"
			+ "	Dies ist ein <a href=\"foo.xml\">Link auf foo.xml</a>, hier auch mit <a href=\"foo.xml#fragment\">Fragment.</a>\n"
			+ "	Eine URL: <url>foo.xml</url>, und zwei: <linkgroup refs=\"foo.xml bar/test.png\"/>.\n" + "</test>";

	private static final String out = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<test>\n"
			+ "	Dies ist ein <a href=\"textgrid:foo\">Link auf textgrid:foo</a>, hier auch mit <a href=\"textgrid:foo#fragment\">Fragment.</a>\n"
			+ "	Eine URL: <url>textgrid:foo</url>, und zwei: <linkgroup refs=\"textgrid:foo textgrid:bar\"></linkgroup>.\n"
			+ "</test>";

	@Test
	public void testRewrite() throws IOException, XMLStreamException {
		final ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:foo", "foo.xml", null, RewriteMethod.TEXT);
		mapping.add("textgrid:bar", "bar/test.png", null, null);
		final XMLLinkRewriter rewriter = new XMLLinkRewriter(mapping, false);

		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		rewriter.rewrite(stringToStream(in), outputStream);
		assertEquals(out, outputStream.toString("UTF-8"));
	}

}
