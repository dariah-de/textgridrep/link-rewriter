package info.textgrid.utils.linkrewriter;

import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid._import.RewriteMethod;

import java.io.StringReader;
import java.net.URI;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import org.junit.Assert;
import org.junit.Test;

public class ImportMappingTest {

	private final String FOOBAR_MAPPING_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<importSpec xmlns=\"http://textgrid.info/import\">\n"
			+ "    <importObject rewrite-method=\"text\" rewrite=\"true\" local-data=\"foo.xml\" textgrid-uri=\"textgrid:foo\"/>\n"
			+ "    <importObject local-data=\"bar.png\" textgrid-uri=\"textgrid:bar\"/>\n" + "</importSpec>\n";

	@Test
	public void unmarshalTest() throws JAXBException {
		final ImportMapping mapping = new ImportMapping();

		final ImportObject foo = new ImportObject();
		foo.setLocalData("foo.xml");
		foo.setTextgridUri("textgrid:foo");
		foo.setRewriteMethod(RewriteMethod.TEXT);

		final ImportObject bar = new ImportObject();
		bar.setLocalData("bar.png");
		bar.setTextgridUri("textgrid:bar");

		mapping.add(foo, bar);

		final JAXBContext context = JAXBContext.newInstance(ImportSpec.class);
		final Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(mapping.toImportSpec(), System.out);

		final Unmarshaller unmarshaller = context.createUnmarshaller();
		final ImportSpec importSpec = (ImportSpec) unmarshaller.unmarshal(new StringReader(FOOBAR_MAPPING_XML));
		final ImportMapping importMapping = new ImportMapping(importSpec);
		marshaller.marshal(importMapping.toImportSpec(), System.out);
	}
	
	@Test
	public void mappingSpaceTest() {
		ImportMapping mapping = new ImportMapping();
		mapping.add("textgrid:space", "ein test", null, RewriteMethod.XML);
		mapping.add("textgrid:percenttwenty", "zweiter%20test", null, RewriteMethod.XML);
		Assert.assertNotNull("Could not retrieve 'ein test'", mapping.getImportObjectForLocalURI(("ein test")));
		Assert.assertNotNull("Could not retrieve 'ein%20test'", mapping.getImportObjectForLocalURI(("ein%20test")));
		Assert.assertNotNull("Could not retrieve 'zweiter test'", mapping.getImportObjectForLocalURI(("zweiter test")));
		Assert.assertNotNull("Could not retrieve 'zweiter%20test'", mapping.getImportObjectForLocalURI(("zweiter test")));
	}

}
